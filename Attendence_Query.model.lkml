connection: "snowflake"

include: "/views/*.view.lkml"                # include all views in the views/ folder in this project
include: "/views/PUBLIC/bi_dim_event_all.view.lkml"
include: "/views/PUBLIC/bi_agg_web_event.view.lkml"
include: "/views/PUBLIC/bi_agg_web_event_adj.view.lkml"
include: "/views/CLIENT_ANALYTICS/bi_fact_cfc_ga_hit_agg.view.lkml"
include: "/views/PUBLIC/ga_mp_hits.view.lkml"
# include: "/**/view.lkml"                   # include all views in this project
# include: "my_dashboard.dashboard.lookml"   # include a LookML dashboard called my_dashboard

# # Select the views that should be a part of this model,
# # and define the joins that connect them together.
#
explore: bi_agg_web_event_adj {
  join: event {
    type: left_outer
    sql_on: ${bi_agg_web_event_adj.event_id} = ${event.event_id} ;;
    relationship: one_to_many
  }
}
explore: bi_agg_web_event {}

explore: bi_dim_event_all {
  join: event {
    type: left_outer
    sql_on: ${bi_dim_event_all.ven_id} = ${event.ven_id}
    and ${bi_dim_event_all.primary_act_id} = ${event.primary_act_id}  ;;
    relationship:  one_to_many
  }
  join: bi_agg_web_event_adj {
    type: left_outer
    sql_on: ${bi_dim_event_all.event_id} = ${bi_agg_web_event_adj.event_id} ;;
    relationship: one_to_many
  }
}
explore: bi_fact_cfc_ga_hit_agg {
  join: ga_mp_hits {
    type: left_outer
    sql_on: ${bi_fact_cfc_ga_hit_agg.cd_event_id} = ${ga_mp_hits.cd_event_id} ;;
    relationship: many_to_one
  }
}

explore: attendance_scan {
  # access_filter: {
  #  field: event.event_id
  #  user_attribute: event_id_hex
  #}
  join: attendance_event_xref {
    type: inner
    sql_on: ${attendance_scan.scan_event_id} = ${attendance_event_xref.scan_event_id} ;;
    relationship: one_to_many
  }

  join: bi_dim_event {
    type: inner
    sql_on: ${bi_dim_event.event_id_hex} = ${attendance_event_xref.event_id_hex}
          and ${bi_dim_event.arch_event_id} = ${attendance_event_xref.arch_event_id}
          and ${bi_dim_event.client_cd} = ${attendance_event_xref.client_cd};;
    relationship: many_to_one
  }
  join: attendance_scan_result {
    type: inner {
      sql_on: ${attendance_scan.scan_result_cd} = ${attendance_scan_result.scan_result_cd}
        AND ${attendance_scan.attendance_src_sys_cd} = ${attendance_scan_result.attendance_src_sys_cd};;
      relationship: one_to_many
    }
  }
  join: attendance_delivery_channel {
    type: inner
    sql_on: ${attendance_scan.delivery_channel_cd} = ${attendance_delivery_channel.delivery_channel_cd} ;;
    relationship: many_to_one

  }
  join: attendance_tkt_status {
    type: left_outer
    sql_on: ${attendance_scan.tkt_status_cd} = ${attendance_tkt_status.tkt_status_cd} ;;
    relationship: many_to_one
  }

}
explore: scan_rate {}

explore: count {}

explore: countchannelcode {}

explore: application_count {}

explore: mobileprintcount {
#  access_filter: {
#   field: mobileprintcount.host_event_id
#    user_attribute: event_id_hex
# }
}
