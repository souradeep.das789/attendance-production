connection: "snowflake"

include: "/views/*.view.lkml"                # include all views in the views/ folder in this project
include: "/views/PUBLIC/bi_dim_event_all.view.lkml"
include: "/views/PUBLIC/bi_agg_web_event_adj.view.lkml"
include: "/views/CLIENT_ANALYTICS/bi_fact_cfc_ga_hit_agg.view.lkml"
include: "/views/PUBLIC/ga_mp_hits.view.lkml"
# include: "/**/view.lkml"                   # include all views in this project
# include: "my_dashboard.dashboard.lookml"   # include a LookML dashboard called my_dashboard

# # Select the views that should be a part of this model,
# # and define the joins that connect them together.
#

datagroup: snowflake_attendance_default_datagroup {
  # sql_trigger: SELECT MAX(id) FROM etl_log;;
  max_cache_age: "1 hour"
}

persist_with: snowflake_attendance_default_datagroup
explore: attendance_delivery_channel {}

explore: attendance_event_status {}

explore: attendance_event_xref {}


#Below Table not their in Snowflake
#explore: attendance_event_xref_shared {}
#explore: bi_agg_web_artist_ga {}
#explore: bi_agg_web_artist_test {}
#explore: bi_agg_web_domain {}
#explore: bi_agg_web_domain_test {}

#explore: bi_agg_web_event_ga {
#  join: event {
#    type: left_outer
#   sql_on: ${bi_agg_web_event_ga.event_id} = ${event.arch_event_id} ;;
#    relationship: many_to_one
#  }
#}

#explore: bi_agg_web_event_test {
#  join: event {
#    type: left_outer
#    sql_on: ${bi_agg_web_event_test.event_id} = ${event.arch_event_id} ;;
#    relationship: many_to_one
#  }
#}

explore: inventory_seat {}

explore: inventory_seat_offer_set {}


explore: attendance_scan {
  join: event {
    type: left_outer
    sql_on: ${attendance_scan.event_id_hex} = ${event.event_id_hex} ;;
    relationship: many_to_one
  }
  join: attendance_scan_result {
    type: inner
    sql_on: ${attendance_scan.scan_result_cd} = ${attendance_scan_result.scan_result_cd} ;;
    relationship: many_to_one
  }
  join: venue {
    type: left_outer
    sql_on:  ${event.ven_id} = ${venue.venue_id} ;;
    relationship: many_to_one
  }
}


explore: attendance_scan_event_vw {
  # access_filter: {
  #  field: host_event_id
  #  user_attribute: event_id_hex
  #}
}

explore: attendance_scan_event_vw1 {
  # access_filter: {
  #  field: bi_dim_event_event_key
  # user_attribute: event_id_hex
#  }
}



explore: event {
  join: bi_dim_event_all {
    type: left_outer
    sql_on: ${event.event_id_hex} = ${bi_dim_event_all.event_id_hex} ;;
    relationship: one_to_many
  }
  join: bi_agg_web_event_adj {
    type: left_outer
    sql_on: ${bi_agg_web_event_adj.event_id} = ${event.event_id} ;;
    relationship: one_to_many
  }
}
#  join: event {
#    type: left_outer
#    sql_on: ${event.event_id} = ${event.arch_event_id} ;;
#    relationship: many_to_one
#  }
#}
