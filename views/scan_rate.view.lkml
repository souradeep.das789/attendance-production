view: scan_rate {
  derived_table: {
    sql: SELECT
          (CASE WHEN TIMEZN_CUR_OFFSET_VAL = NULL THEN SCAN_DTTM_UTC ELSE DATEADD(hour,TIMEZN_CUR_OFFSET_VAL , SCAN_DTTM_UTC::timestampntz) END) AS "ConvertedTimeZone",
        evt.EVENT_ID_HEX,
        evt.EVENT_ID AS HOST_EVENT_ID,
        evt.CLIENT_CD,
        evt.ARCH_EVENT_ID,
          evt.EVENT_CODE AS HOST_EVENT_CODE,
          evt.ARCH_EVENT_CODE,
        scan.ATTENDANCE_SRC_SYS_CD,
        scan.ORDER_SYSTEM_SRC_SYS_CD,
        evt.EVENT_ID_SRC_SYS_CD,
        scan.EVENT_CD AS SCAN_EVENT_CD,
        scan.DISC_VEN_ID,
        scan.CLIENT_NM,
        --evt_status.GHOST_IND,
        --evt_status.PRIMARY_INGRESS_IND,
        scan.SCAN_DTTM_UTC,
        scan.BARCODE,
        scan.SCAN_SEQ_NUM,
        scan.SCAN_DT_UTC,
        scan_result.SCAN_ENTRY_CAT,
        scan_result.SCAN_RESULT_CAT,
        scan.SCAN_RESULT_CD,
        scan_result.SCAN_RESULT_NM,
        scan_result.SCAN_RESULT_DESC,
        scan_result.ONLINE_IND,
        scan_result.ARCHTICS_RESULT_CD,
        scan.REJECT_CD,
        scan.REJECT_REASON_NM,
        scan.TKT_STATUS_CD,
        tkt_status.TKT_STATUS_NM,
        scan.SECT_NM,
        scan.ROW_NUM,
        scan.SEAT_NUM,
        scan.SEAT_CD,
        scan.BASE_TKT_TYPE_ID,
        scan.GATE_ID,
        scan.GATE_NM,
        scan.SCANNER_DEVICE_ID,
        scan.SCANNER_DEVICE_NM,
        scan.SCANNER_SOFTWARE_VERSION_NUM,
        scan.FAN_DEVICE_OS_NM,
        scan.CUST_ID,
        scan.PRINT_CNT,
        scan.ACCESS_MGR_SYS_ID,
        scan.SCAN_TOKEN_ID,
        scan.DELIVERY_CHANNEL_CD,
        delivery.DELIVERY_CHANNEL_CAT,
        delivery.DELIVERY_CHANNEL_NM,
        delivery.DELIVERY_CHANNEL_DESC,
        scan.MSG_UPLOAD_DTTM,
        scan.TICKET_ID,
          evt.Row_Sys_Id,
          evt.Event_Name,
          evt.Event_Disp_Name,
          evt.Event_Type_Cd,
          evt.Event_Type_Desc,
          evt.Major_Cat_Name,
          evt.Minor_Cat_Name,
          evt.primary_act_id,
          evt.primary_act_Name,
          evt.secondary_act_id,
          evt.secondary_act_Name,
          evt.Promoter_Name,
          evt.Ven_Id,
          evt.Ven_Id_Src_Sys_Cd,
          evt.Event_Dt,
          evt.event_time,
          evt.event_date_time_utc,
          evt.Onsale_Dt,
          evt.Presale_Dt,
          evt.ClientBP,
          evt.ISM_Enabled_Flag,
          evt.Cancelled_Flg,
          evt.Settle_Cd,
          evt.Zone_Cd,
          evt.Exch_Active_Flg,
          evt.Cur_Ind,
          evt.Create_Dt,
          evt.Venue_Name,
          evt.Venue_City,
          evt.Venue_State,
          evt.Venue_zipcode,
          evt.Nrep_Music_Service_Flg,
          evt.Digital_Event_Flg,
          evt.Valid_Ticketed_Event_Flag,
          evt.Ticket_Text,
          scan.SCAN_EVENT_ID,
        EVT.TIMEZN_CUR_OFFSET_VAL,
          --es.GHOST_IND,
          --es.PRIMARY_INGRESS_IND,
          NULL AS GHOST_IND,
          NULL AS PRIMARY_INGRESS_IND,
          scan.INSERT_TS,
          scan.UPDATE_TS
      FROM
        BI.PUBLIC.ATTENDANCE_SCAN scan
        INNER JOIN BI.PUBLIC.ATTENDANCE_EVENT_XREF X
              ON scan.SCAN_EVENT_ID = x.SCAN_EVENT_ID
          --LEFT OUTER JOIN BI.PUBLIC.ATTENDANCE_EVENT_STATUS es
          --ON es.EVENT_ID_HEX = x.EVENT_ID_HEX
          --AND es.ARCH_EVENT_ID = x.ARCH_EVENT_ID
          --AND es.CLIENT_CD = x.CLIENT_CD
              --AND es.EVENT_CD = scan.EVENT_CD
              --AND es.ATTENDANCE_SRC_SYS_CD = scan.ATTENDANCE_SRC_SYS_CD
          INNER JOIN BI.PUBLIC.EVENT evt
          ON evt.EVENT_ID_HEX = x.EVENT_ID_HEX
          AND evt.ARCH_EVENT_ID = x.ARCH_EVENT_ID
          AND evt.CLIENT_CD = x.CLIENT_CD
        INNER JOIN BI.PUBLIC.ATTENDANCE_SCAN_RESULT scan_result
          ON scan.SCAN_RESULT_CD = scan_result.SCAN_RESULT_CD
          AND scan.ATTENDANCE_SRC_SYS_CD = scan_result.ATTENDANCE_SRC_SYS_CD
        INNER JOIN BI.PUBLIC.ATTENDANCE_DELIVERY_CHANNEL delivery
              ON scan.DELIVERY_CHANNEL_CD = delivery.DELIVERY_CHANNEL_CD
          LEFT OUTER JOIN BI.PUBLIC.ATTENDANCE_TKT_STATUS tkt_status
          ON scan.TKT_STATUS_CD = tkt_status.TKT_STATUS_CD
          AND scan.ATTENDANCE_SRC_SYS_CD = tkt_status.ATTENDANCE_SRC_SYS_CD
       ;;
  }

  measure: count {
    type: count
    drill_fields: [detail*]
  }

  # dimension_group: converted_time_zone {
  #  type: time
  # sql: ${TABLE}."ConvertedTimeZone" ;;
  #}

  dimension: event_id_hex {
    type: string
    sql: ${TABLE}."EVENT_ID_HEX" ;;
  }

  dimension: host_event_id {
    type: string
    sql: ${TABLE}."HOST_EVENT_ID" ;;
  }

  dimension: client_cd {
    type: string
    sql: ${TABLE}."CLIENT_CD" ;;
  }

  dimension: arch_event_id {
    type: string
    sql: ${TABLE}."ARCH_EVENT_ID" ;;
  }

  dimension: host_event_code {
    type: string
    sql: ${TABLE}."HOST_EVENT_CODE" ;;
  }

  dimension: arch_event_code {
    type: string
    sql: ${TABLE}."ARCH_EVENT_CODE" ;;
  }

  dimension: attendance_src_sys_cd {
    type: string
    sql: ${TABLE}."ATTENDANCE_SRC_SYS_CD" ;;
  }

  dimension: order_system_src_sys_cd {
    type: string
    sql: ${TABLE}."ORDER_SYSTEM_SRC_SYS_CD" ;;
  }

  dimension: event_id_src_sys_cd {
    type: string
    sql: ${TABLE}."EVENT_ID_SRC_SYS_CD" ;;
  }

  dimension: scan_event_cd {
    type: string
    sql: ${TABLE}."SCAN_EVENT_CD" ;;
  }

  dimension: disc_ven_id {
    type: string
    sql: ${TABLE}."DISC_VEN_ID" ;;
  }

  dimension: client_nm {
    type: string
    sql: ${TABLE}."CLIENT_NM" ;;
  }

  dimension_group: scan_dttm_utc {
    type: time
    sql: ${TABLE}."SCAN_DTTM_UTC" ;;
  }

  dimension: barcode {
    type: string
    sql: ${TABLE}."BARCODE" ;;
  }

  dimension: scan_seq_num {
    type: number
    sql: ${TABLE}."SCAN_SEQ_NUM" ;;
  }

  dimension: scan_dt_utc {
    type: date
    sql: ${TABLE}."SCAN_DT_UTC" ;;
  }

  dimension: scan_entry_cat {
    type: string
    sql: ${TABLE}."SCAN_ENTRY_CAT" ;;
  }

  dimension: scan_result_cat {
    type: string
    sql: ${TABLE}."SCAN_RESULT_CAT" ;;
  }

  dimension: scan_result_cd {
    type: string
    sql: ${TABLE}."SCAN_RESULT_CD" ;;
  }

  dimension: scan_result_nm {
    type: string
    sql: ${TABLE}."SCAN_RESULT_NM" ;;
  }

  dimension: scan_result_desc {
    type: string
    sql: ${TABLE}."SCAN_RESULT_DESC" ;;
  }

  dimension: online_ind {
    type: string
    sql: ${TABLE}."ONLINE_IND" ;;
  }

  dimension: archtics_result_cd {
    type: number
    sql: ${TABLE}."ARCHTICS_RESULT_CD" ;;
  }

  dimension: reject_cd {
    type: string
    sql: ${TABLE}."REJECT_CD" ;;
  }

  dimension: reject_reason_nm {
    type: string
    sql: ${TABLE}."REJECT_REASON_NM" ;;
  }

  dimension: tkt_status_cd {
    type: string
    sql: ${TABLE}."TKT_STATUS_CD" ;;
  }

  dimension: tkt_status_nm {
    type: string
    sql: ${TABLE}."TKT_STATUS_NM" ;;
  }

  dimension: sect_nm {
    type: string
    sql: ${TABLE}."SECT_NM" ;;
  }

  dimension: row_num {
    type: string
    sql: ${TABLE}."ROW_NUM" ;;
  }

  dimension: seat_num {
    type: string
    sql: ${TABLE}."SEAT_NUM" ;;
  }

  dimension: seat_cd {
    type: string
    sql: ${TABLE}."SEAT_CD" ;;
  }

  dimension: base_tkt_type_id {
    type: string
    sql: ${TABLE}."BASE_TKT_TYPE_ID" ;;
  }

  dimension: gate_id {
    type: string
    sql: ${TABLE}."GATE_ID" ;;
  }

  dimension: gate_nm {
    type: string
    sql: ${TABLE}."GATE_NM" ;;
  }

  dimension: scanner_device_id {
    type: string
    sql: ${TABLE}."SCANNER_DEVICE_ID" ;;
  }

  dimension: scanner_device_nm {
    type: string
    sql: ${TABLE}."SCANNER_DEVICE_NM" ;;
  }

  dimension: scanner_software_version_num {
    type: string
    sql: ${TABLE}."SCANNER_SOFTWARE_VERSION_NUM" ;;
  }

  dimension: fan_device_os_nm {
    type: string
    sql: ${TABLE}."FAN_DEVICE_OS_NM" ;;
  }

  dimension: cust_id {
    type: string
    sql: ${TABLE}."CUST_ID" ;;
  }

  dimension: print_cnt {
    type: number
    sql: ${TABLE}."PRINT_CNT" ;;
  }

  dimension: access_mgr_sys_id {
    type: string
    sql: ${TABLE}."ACCESS_MGR_SYS_ID" ;;
  }

  dimension: scan_token_id {
    type: string
    sql: ${TABLE}."SCAN_TOKEN_ID" ;;
  }

  dimension: delivery_channel_cd {
    type: string
    sql: ${TABLE}."DELIVERY_CHANNEL_CD" ;;
  }

  dimension: delivery_channel_cat {
    type: string
    sql: ${TABLE}."DELIVERY_CHANNEL_CAT" ;;
  }

  dimension: delivery_channel_nm {
    type: string
    sql: ${TABLE}."DELIVERY_CHANNEL_NM" ;;
  }

  dimension: delivery_channel_desc {
    type: string
    sql: ${TABLE}."DELIVERY_CHANNEL_DESC" ;;
  }

  dimension_group: msg_upload_dttm {
    type: time
    sql: ${TABLE}."MSG_UPLOAD_DTTM" ;;
  }

  dimension: ticket_id {
    type: string
    sql: ${TABLE}."TICKET_ID" ;;
  }

  dimension: row_sys_id {
    type: number
    sql: ${TABLE}."ROW_SYS_ID" ;;
  }

  dimension: event_name {
    type: string
    sql: ${TABLE}."EVENT_NAME" ;;
  }

  dimension: event_disp_name {
    type: string
    sql: ${TABLE}."EVENT_DISP_NAME" ;;
  }

  dimension: event_type_cd {
    type: string
    sql: ${TABLE}."EVENT_TYPE_CD" ;;
  }

  dimension: event_type_desc {
    type: string
    sql: ${TABLE}."EVENT_TYPE_DESC" ;;
  }

  dimension: major_cat_name {
    type: string
    sql: ${TABLE}."MAJOR_CAT_NAME" ;;
  }

  dimension: minor_cat_name {
    type: string
    sql: ${TABLE}."MINOR_CAT_NAME" ;;
  }

  dimension: primary_act_id {
    type: number
    sql: ${TABLE}."PRIMARY_ACT_ID" ;;
  }

  dimension: primary_act_name {
    type: string
    sql: ${TABLE}."PRIMARY_ACT_NAME" ;;
  }

  dimension: secondary_act_id {
    type: number
    sql: ${TABLE}."SECONDARY_ACT_ID" ;;
  }

  dimension: secondary_act_name {
    type: string
    sql: ${TABLE}."SECONDARY_ACT_NAME" ;;
  }

  dimension: promoter_name {
    type: string
    sql: ${TABLE}."PROMOTER_NAME" ;;
  }

  dimension: ven_id {
    type: number
    sql: ${TABLE}."VEN_ID" ;;
  }

  dimension: ven_id_src_sys_cd {
    type: string
    sql: ${TABLE}."VEN_ID_SRC_SYS_CD" ;;
  }

  dimension: event_dt {
    type: date
    sql: ${TABLE}."EVENT_DT" ;;
  }

  dimension: event_time {
    type: number
    sql: ${TABLE}."EVENT_TIME" ;;
  }

  dimension_group: event_date_time_utc {
    type: time
    sql: ${TABLE}."EVENT_DATE_TIME_UTC" ;;
  }

  dimension: onsale_dt {
    type: date
    sql: ${TABLE}."ONSALE_DT" ;;
  }

  dimension: presale_dt {
    type: date
    sql: ${TABLE}."PRESALE_DT" ;;
  }

  dimension: clientbp {
    type: number
    sql: ${TABLE}."CLIENTBP" ;;
  }

  dimension: ism_enabled_flag {
    type: string
    sql: ${TABLE}."ISM_ENABLED_FLAG" ;;
  }

  dimension: cancelled_flg {
    type: string
    sql: ${TABLE}."CANCELLED_FLG" ;;
  }

  dimension: settle_cd {
    type: string
    sql: ${TABLE}."SETTLE_CD" ;;
  }

  dimension: zone_cd {
    type: number
    sql: ${TABLE}."ZONE_CD" ;;
  }

  dimension: exch_active_flg {
    type: string
    sql: ${TABLE}."EXCH_ACTIVE_FLG" ;;
  }

  dimension: cur_ind {
    type: string
    sql: ${TABLE}."CUR_IND" ;;
  }

  dimension: create_dt {
    type: date
    sql: ${TABLE}."CREATE_DT" ;;
  }

  dimension: venue_name {
    type: string
    sql: ${TABLE}."VENUE_NAME" ;;
  }

  dimension: venue_city {
    type: string
    sql: ${TABLE}."VENUE_CITY" ;;
  }

  dimension: venue_state {
    type: string
    sql: ${TABLE}."VENUE_STATE" ;;
  }

  dimension: venue_zipcode {
    type: string
    sql: ${TABLE}."VENUE_ZIPCODE" ;;
  }

  dimension: nrep_music_service_flg {
    type: string
    sql: ${TABLE}."NREP_MUSIC_SERVICE_FLG" ;;
  }

  dimension: digital_event_flg {
    type: string
    sql: ${TABLE}."DIGITAL_EVENT_FLG" ;;
  }

  dimension: valid_ticketed_event_flag {
    type: string
    sql: ${TABLE}."VALID_TICKETED_EVENT_FLAG" ;;
  }

  dimension: ticket_text {
    type: string
    sql: ${TABLE}."TICKET_TEXT" ;;
  }

  dimension: scan_event_id {
    type: string
    sql: ${TABLE}."SCAN_EVENT_ID" ;;
  }

  dimension: timezn_cur_offset_val {
    type: number
    sql: ${TABLE}."TIMEZN_CUR_OFFSET_VAL" ;;
  }

  dimension: ghost_ind {
    type: string
    sql: ${TABLE}."GHOST_IND" ;;
  }

  dimension: primary_ingress_ind {
    type: string
    sql: ${TABLE}."PRIMARY_INGRESS_IND" ;;
  }

  dimension_group: insert_ts {
    type: time
    sql: ${TABLE}."INSERT_TS" ;;
  }

  dimension_group: update_ts {
    type: time
    sql: ${TABLE}."UPDATE_TS" ;;
  }
  dimension: Entry {
    type: number
    sql: ${scan_entry_cat} = 'ENTRY' ;;
  }
  dimension_group: scan_rate {
    type:  time
    timeframes: [minute, minute10, minute15, minute30]
    sql: ${converted_time_zone_raw} ;;
    convert_tz: yes
  }
  dimension: is_entry_scan {
    type: yesno
    sql: ${scan_entry_cat} IN ('ENTRY') ;;
  }

  measure: total_entry{
    label: "Total Entry"
    type: count
    filters: {
      field: is_entry_scan
      value: "yes"

    }
  }
  dimension: is_reentry_scan {
    type: yesno
    sql: ${scan_entry_cat} IN ( 'REENTRY') ;;
  }

  measure: total_Reentry {
    label: "Total Reentry"
    type: count
    filters: {
      field: is_reentry_scan
      value: "yes"

    }
  }

  measure: In {
    label: "In"
    type: number
    sql: ${total_Reentry} + ${total_entry} ;;
  }
  measure: total_scans_in {
    label: "Total Scans In"
    type: count
    filters: {
      field: is_entry_scan
      value: "yes"

    }
  }
  dimension: Rejects {
    type: yesno
    sql: ${scan_entry_cat} IN ('REJECT', 'EXIT_REJECT', 'INTERNAL_EXIT_REJECT','INTERNAL_REJECT')  ;;
  }

  measure: RejectsIn {
    label: "Rejects"
    type: count
    filters: {
      field: Rejects
      value: "yes"

    }
  }

  measure: Difference {
    label: "Diff"
    type: number
    sql: ${Rejects} / ${total_scans_in} ;;
  }
  dimension_group: converted_time_zone {
    type: time
    timeframes: [
      raw,
      time,
      date,
      week,
      month,
      quarter,
      year
    ]
    sql: ${TABLE}."ConvertedTimeZone" ;;
  }

  dimension: TempScansIn {
    type: string
    sql:case when ${scan_entry_cat} IN ('ENTRY', 'REENTRY') then ${barcode} end ;;
  }


  dimension: Safetix {
    type: string
    sql: case when ${fan_device_os_nm}  LIKE '%rotating%'  then 'Yes'
        else
        case when ${fan_device_os_nm} IN ('android' , 'ANDROID', 'IOS', 'ios') then 'Yes'
        else 'No'
        end
        end;;
        #sql: ${fan_device_os_nm} LIKE '%rotating%' AND ${fan_device_os_nm} IN ('android' , 'ANDROID', 'IOS', 'ios')  ;;
    }
    # dimension_group: converted_time_zone {
    #  type: time
    # sql: ${TABLE}."ConvertedTimeZone" ;;
    #}
    set: detail {
      fields: [
        converted_time_zone_time,
        event_id_hex,
        host_event_id,
        client_cd,
        arch_event_id,
        host_event_code,
        arch_event_code,
        attendance_src_sys_cd,
        order_system_src_sys_cd,
        event_id_src_sys_cd,
        scan_event_cd,
        disc_ven_id,
        client_nm,
        scan_dttm_utc_time,
        barcode,
        scan_seq_num,
        scan_dt_utc,
        scan_entry_cat,
        scan_result_cat,
        scan_result_cd,
        scan_result_nm,
        scan_result_desc,
        online_ind,
        archtics_result_cd,
        reject_cd,
        reject_reason_nm,
        tkt_status_cd,
        tkt_status_nm,
        sect_nm,
        row_num,
        seat_num,
        seat_cd,
        base_tkt_type_id,
        gate_id,
        gate_nm,
        scanner_device_id,
        scanner_device_nm,
        scanner_software_version_num,
        fan_device_os_nm,
        cust_id,
        print_cnt,
        access_mgr_sys_id,
        scan_token_id,
        delivery_channel_cd,
        delivery_channel_cat,
        delivery_channel_nm,
        delivery_channel_desc,
        msg_upload_dttm_time,
        ticket_id,
        row_sys_id,
        event_name,
        event_disp_name,
        event_type_cd,
        event_type_desc,
        major_cat_name,
        minor_cat_name,
        primary_act_id,
        primary_act_name,
        secondary_act_id,
        secondary_act_name,
        promoter_name,
        ven_id,
        ven_id_src_sys_cd,
        event_dt,
        event_time,
        event_date_time_utc_time,
        onsale_dt,
        presale_dt,
        clientbp,
        ism_enabled_flag,
        cancelled_flg,
        settle_cd,
        zone_cd,
        exch_active_flg,
        cur_ind,
        create_dt,
        venue_name,
        venue_city,
        venue_state,
        venue_zipcode,
        nrep_music_service_flg,
        digital_event_flg,
        valid_ticketed_event_flag,
        ticket_text,
        scan_event_id,
        timezn_cur_offset_val,
        ghost_ind,
        primary_ingress_ind,
        insert_ts_time,
        update_ts_time
      ]
    }
  }
