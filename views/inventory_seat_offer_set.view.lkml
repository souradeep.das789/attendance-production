view: inventory_seat_offer_set {
  sql_table_name: "CLIENT_ANALYTICS"."INVENTORY_SEAT_OFFER_SET"
    ;;

  dimension: event_id {
    type: string
    sql: ${TABLE}."EVENT_ID" ;;
  }

  dimension_group: insert_ts {
    type: time
    timeframes: [
      raw,
      time,
      date,
      week,
      month,
      quarter,
      year
    ]
    sql: ${TABLE}."INSERT_TS" ;;
  }

  dimension_group: msg_upload_ts {
    type: time
    timeframes: [
      raw,
      time,
      date,
      week,
      month,
      quarter,
      year
    ]
    sql: ${TABLE}."MSG_UPLOAD_TS" ;;
  }

  dimension: offer_set_id {
    type: string
    sql: ${TABLE}."OFFER_SET_ID" ;;
  }

  dimension: place_id {
    type: string
    sql: ${TABLE}."PLACE_ID" ;;
  }

  dimension: restricted_ind {
    type: yesno
    sql: ${TABLE}."RESTRICTED_IND" ;;
  }

  dimension_group: update_ts {
    type: time
    timeframes: [
      raw,
      time,
      date,
      week,
      month,
      quarter,
      year
    ]
    sql: ${TABLE}."UPDATE_TS" ;;
  }

  measure: count {
    type: count
    drill_fields: []
  }
}
