view: application_count {
  derived_table: {
    sql: SELECT
        CASE
                         WHEN (attendance_scan_event_vw."DELIVERY_CHANNEL_CD") = 'A' THEN 'Ticketmaster'
                         WHEN (attendance_scan_event_vw."DELIVERY_CHANNEL_CD") = 'B' THEN 'Live Nation'
                         WHEN (attendance_scan_event_vw."DELIVERY_CHANNEL_CD") = 'C' THEN 'Ticketmaster'
                         WHEN (attendance_scan_event_vw."DELIVERY_CHANNEL_CD") = 'D' THEN 'Live Nation'
                         WHEN (attendance_scan_event_vw."DELIVERY_CHANNEL_CD") = 'E' THEN 'Ticketmaster'
                         WHEN (attendance_scan_event_vw."DELIVERY_CHANNEL_CD") = 'F' THEN 'Live Nation'
                         WHEN (attendance_scan_event_vw."DELIVERY_CHANNEL_CD") = 'G' THEN 'Wallet'
                         WHEN (attendance_scan_event_vw."DELIVERY_CHANNEL_CD") = 'H' THEN 'Goldstar'
                         WHEN (attendance_scan_event_vw."DELIVERY_CHANNEL_CD") = 'I' THEN 'Gametime'
                         WHEN (attendance_scan_event_vw."DELIVERY_CHANNEL_CD") = 'J' THEN 'Account Manager Web'
                         WHEN (attendance_scan_event_vw."DELIVERY_CHANNEL_CD") = 'K' THEN 'Account Manager Web'
                         WHEN (attendance_scan_event_vw."DELIVERY_CHANNEL_CD") = 'L' THEN 'Wallet'
                         WHEN (attendance_scan_event_vw."DELIVERY_CHANNEL_CD") = 'M' THEN 'Presence SDK'
                         WHEN (attendance_scan_event_vw."DELIVERY_CHANNEL_CD") = 'N' THEN 'Presence SDK'
                         WHEN (attendance_scan_event_vw."DELIVERY_CHANNEL_CD") = 'O' THEN 'Presence SDK'
                         WHEN (attendance_scan_event_vw."DELIVERY_CHANNEL_CD") = 'P' THEN 'Groupon'
                         WHEN (attendance_scan_event_vw."DELIVERY_CHANNEL_CD") = 'PRINT' THEN 'Printed'
                         WHEN (attendance_scan_event_vw."DELIVERY_CHANNEL_CD") = 'Q' THEN 'StubHub'
                         WHEN (attendance_scan_event_vw."DELIVERY_CHANNEL_CD") = 'R' THEN 'Wallet'
                         WHEN (attendance_scan_event_vw."DELIVERY_CHANNEL_CD") = 'S' THEN 'SeatGeek'
                         WHEN (attendance_scan_event_vw."DELIVERY_CHANNEL_CD") = 'T' THEN 'TM1 Sales'
                         WHEN (attendance_scan_event_vw."DELIVERY_CHANNEL_CD") = 'U' THEN 'Archtics SMS'
                         WHEN (attendance_scan_event_vw."DELIVERY_CHANNEL_CD") = 'V' THEN 'Venue'
                         WHEN (attendance_scan_event_vw."DELIVERY_CHANNEL_CD") = 'W' THEN 'Wristband'
                         WHEN (attendance_scan_event_vw."DELIVERY_CHANNEL_CD") = 'X' THEN 'MLB'
                         WHEN (attendance_scan_event_vw."DELIVERY_CHANNEL_CD") = 'Y' THEN 'Wallet'
                         WHEN (attendance_scan_event_vw."DELIVERY_CHANNEL_CD") = 'Z' THEN 'GameDay'
                         WHEN (attendance_scan_event_vw."DELIVERY_CHANNEL_CD") = '@' THEN 'Wallet'
                         WHEN (attendance_scan_event_vw."DELIVERY_CHANNEL_CD") = '^' THEN 'VividSeats'
                         WHEN (attendance_scan_event_vw."DELIVERY_CHANNEL_CD") = '&' THEN 'TM1 Sales'
                         WHEN (attendance_scan_event_vw."DELIVERY_CHANNEL_CD") = '+' THEN 'Booking.com'
                         ELSE 'Unknown'
                  END  AS "attendance_scan_event_vw.em_application",
        COUNT(attendance_scan_event_vw."TICKET_ID") AS "attendance_scan_event_vw.total_attendance"
      FROM "PUBLIC"."ATTENDANCE_SCAN_EVENT_VW"
           AS attendance_scan_event_vw
              INNER JOIN
          (SELECT
              se.ticket_id,
           scan_event_id,
          MAX(se.SCAN_DTTM_UTC) AS lastValidScanDate
          FROM
              BI.PUBLIC.ATTENDANCE_SCAN_EVENT_VW se
          WHERE
            {% condition event_name %} se.event_name  {% endcondition %}
    AND se.SCAN_ENTRY_CAT IN ('ENTRY' , 'REENTRY', 'EXIT', 'INTERNAL', 'INTERNAL_ENTRY', 'INTERNAL_ENTRY_EXIT')
    and {% condition event_date %} se.event_dt  {% endcondition %}
    AND se.ATTENDANCE_SRC_SYS_CD = 'PRESENCE'

    and {% condition venuename %} se.Venue_Name  {% endcondition %}


    and {% condition eventtypedesc %} se.Event_Type_Desc  {% endcondition %}


    and {% condition majorcatname %} se.Major_Cat_Name  {% endcondition %}

    and {% condition minorcatname %} se.Minor_Cat_Name  {% endcondition %}


          AND se.ATTENDANCE_SRC_SYS_CD = 'PRESENCE'
          GROUP BY 1,2
          ) AS lastScan
          ON attendance_scan_event_vw.scan_event_id = lastScan.scan_event_id
              AND attendance_scan_event_vw.ticket_id = lastScan.ticket_id
              AND attendance_scan_event_vw.SCAN_DTTM_UTC = lastScan.lastValidScanDate
      GROUP BY 1
       ;;
  }
  filter: event_name {
    type: string
  }

  filter: event_date {
    type: date

  }
  filter: venuename {
    type: string
  }

  filter: safetix {
    type: string
    default_value: "Yes"
  }

  filter: eventtypedesc {
    type: string

  }
  filter: majorcatname {
    type: string

  }
  filter: minorcatname {
    type: string

  }
  measure: count {
    type: count
    drill_fields: [detail*]
  }

  dimension: attendance_scan_event_vw_em_application {
    type: string
    sql: ${TABLE}."attendance_scan_event_vw.em_application" ;;
  }

  dimension: attendance_scan_event_vw_total_attendance {
    type: number
    sql: ${TABLE}."attendance_scan_event_vw.total_attendance" ;;
  }

  set: detail {
    fields: [attendance_scan_event_vw_em_application, attendance_scan_event_vw_total_attendance]
  }
}
