view: attendance_tkt_status {
  sql_table_name: "PUBLIC"."ATTENDANCE_TKT_STATUS"
    ;;

  dimension: attendance_src_sys_cd {
    type: string
    sql: ${TABLE}."ATTENDANCE_SRC_SYS_CD" ;;
  }

  dimension_group: insert_ts {
    type: time
    timeframes: [
      raw,
      time,
      date,
      week,
      month,
      quarter,
      year
    ]
    sql: ${TABLE}."INSERT_TS" ;;
  }

  dimension: tkt_status_cd {
    type: string
    sql: ${TABLE}."TKT_STATUS_CD" ;;
  }

  dimension: tkt_status_nm {
    type: string
    sql: ${TABLE}."TKT_STATUS_NM" ;;
  }

  dimension_group: update_ts {
    type: time
    timeframes: [
      raw,
      time,
      date,
      week,
      month,
      quarter,
      year
    ]
    sql: ${TABLE}."UPDATE_TS" ;;
  }

  measure: count {
    type: count
    drill_fields: []
  }
}
