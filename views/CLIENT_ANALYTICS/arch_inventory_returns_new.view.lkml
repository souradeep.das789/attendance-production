view: arch_inventory_returns_new {
  sql_table_name: "CLIENT_ANALYTICS"."ARCH_INVENTORY_RETURNS_NEW"
    ;;

  dimension_group: add_datetime {
    type: time
    timeframes: [
      raw,
      time,
      date,
      week,
      month,
      quarter,
      year
    ]
    sql: ${TABLE}."ADD_DATETIME" ;;
  }

  dimension: archtics_comp {
    type: number
    sql: ${TABLE}."ARCHTICS_COMP" ;;
  }

  dimension: archtics_hold {
    type: number
    sql: ${TABLE}."ARCHTICS_HOLD" ;;
  }

  dimension: archtics_inquiry {
    type: number
    sql: ${TABLE}."ARCHTICS_INQUIRY" ;;
  }

  dimension: archtics_kill {
    type: number
    sql: ${TABLE}."ARCHTICS_KILL" ;;
  }

  dimension: archtics_open {
    type: number
    sql: ${TABLE}."ARCHTICS_OPEN" ;;
  }

  dimension: archtics_sold {
    type: number
    sql: ${TABLE}."ARCHTICS_SOLD" ;;
  }

  dimension: archtics_ticket_price {
    type: number
    sql: ${TABLE}."ARCHTICS_TICKET_PRICE" ;;
  }

  dimension: dsn {
    type: string
    sql: ${TABLE}."DSN" ;;
  }

  dimension: event_id {
    type: number
    # hidden: yes
    sql: ${TABLE}."EVENT_ID" ;;
  }

  dimension: preprint {
    type: number
    sql: ${TABLE}."PREPRINT" ;;
  }

  dimension_group: return_datetime {
    type: time
    timeframes: [
      raw,
      time,
      date,
      week,
      month,
      quarter,
      year
    ]
    sql: ${TABLE}."RETURN_DATETIME" ;;
  }

  measure: count {
    type: count
    drill_fields: [detail*]
  }

  # ----- Sets of fields for drilling ------
  set: detail {
    fields: [
      event.primary_act_name,
      event.event_disp_name,
      event.promoter_name,
      event.secondary_act_name,
      event.event_id,
      event.event_name,
      event.major_cat_name,
      event.venue_name,
      event.minor_cat_name
    ]
  }
}
