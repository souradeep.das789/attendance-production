view: bi_fact_cfc_ga_hit_agg {
  derived_table: {
    sql: SELECT * FROM "CLIENT_ANALYTICS"."BI_FACT_CFC_GA_HIT_AGG"
      ;;
  }

  measure: count {
    type: count
    drill_fields: [detail*]
  }

  dimension: cd_event_id {
    type: string
    sql: ${TABLE}."CD_EVENT_ID" ;;
  }

  dimension: cd_came_from_code {
    type: string
    sql: ${TABLE}."CD_CAME_FROM_CODE" ;;
  }

  dimension: total_product_price {
    type: number
    sql: ${TABLE}."TOTAL_PRODUCT_PRICE" ;;
  }

  dimension: total_product_quantity {
    type: number
    sql: ${TABLE}."TOTAL_PRODUCT_QUANTITY" ;;
  }

  dimension: total_product_revenue {
    type: number
    sql: ${TABLE}."TOTAL_PRODUCT_REVENUE" ;;
  }

  dimension: total_local_product_revenue {
    type: number
    sql: ${TABLE}."TOTAL_LOCAL_PRODUCT_REVENUE" ;;
  }

  dimension_group: cfc_min_hit_date_and_time {
    type: time
    sql: ${TABLE}."CFC_MIN_HIT_DATE_AND_TIME" ;;
  }

  dimension_group: cfc_max_hit_date_and_time {
    type: time
    sql: ${TABLE}."CFC_MAX_HIT_DATE_AND_TIME" ;;
  }

  dimension_group: min_hit_date_and_time {
    type: time
    sql: ${TABLE}."MIN_HIT_DATE_AND_TIME" ;;
  }

  dimension_group: max_hit_date_and_time {
    type: time
    sql: ${TABLE}."MAX_HIT_DATE_AND_TIME" ;;
  }

    dimension_group: visit_start_dt {
      type: time
      timeframes: [
        raw,
        date,
        week,
        month,
        quarter,
        year
      ]
      convert_tz: no
      datatype: date
      sql: ${TABLE}."VISIT_START_DT" ;;
    }

  dimension: total_rows {
    type: number
    sql: ${TABLE}."TOTAL_ROWS" ;;
  }

  dimension: total_orders {
    type: number
    sql: ${TABLE}."TOTAL_ORDERS" ;;
  }

  dimension: ecommerce_action_type {
    type: string
    sql: ${TABLE}."ECOMMERCE_ACTION_TYPE" ;;
  }

  dimension: product_variant_counter {
    type: number
    sql: ${TABLE}."PRODUCT_VARIANT_COUNTER" ;;
  }

  dimension: cd_resale_flag {
    type: yesno
    sql: ${TABLE}."CD_RESALE_FLAG" ;;
  }

  dimension: cd_edp_visit_first_hit_flag {
    type: yesno
    sql: ${TABLE}."CD_EDP_VISIT_FIRST_HIT_FLAG" ;;
  }

  dimension: cd_session_first_hit_flag {
    type: yesno
    sql: ${TABLE}."CD_SESSION_FIRST_HIT_FLAG" ;;
  }

  dimension_group: insert_ts {
    type: time
    timeframes: [
      raw,
      time,
      date,
      week,
      month,
      quarter,
      year
    ]
    sql: ${TABLE}."INSERT_TS" ;;
  }

  set: detail {
    fields: [
      cd_event_id,
      cd_came_from_code,
      total_product_price,
      total_product_quantity,
      total_product_revenue,
      total_local_product_revenue,
      cfc_min_hit_date_and_time_time,
      cfc_max_hit_date_and_time_time,
      min_hit_date_and_time_time,
      max_hit_date_and_time_time,
      total_rows,
      total_orders,
      ecommerce_action_type,
      product_variant_counter,
      cd_resale_flag,
      cd_edp_visit_first_hit_flag,
      cd_session_first_hit_flag,
      insert_ts_time
    ]
  }
}
