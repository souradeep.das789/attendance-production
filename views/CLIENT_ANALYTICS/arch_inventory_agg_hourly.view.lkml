view: arch_inventory_agg_hourly {
  sql_table_name: "CLIENT_ANALYTICS"."ARCH_INVENTORY_AGG_HOURLY"
    ;;

  dimension: archtics_comp {
    type: number
    sql: ${TABLE}."ARCHTICS_COMP" ;;
  }

  dimension: archtics_sold {
    type: number
    sql: ${TABLE}."ARCHTICS_SOLD" ;;
  }

  dimension: archtics_ticket_price {
    type: number
    sql: ${TABLE}."ARCHTICS_TICKET_PRICE" ;;
  }

  dimension_group: collection_ts {
    type: time
    timeframes: [
      raw,
      time,
      date,
      week,
      month,
      quarter,
      year
    ]
    sql: ${TABLE}."COLLECTION_TS" ;;
  }

  dimension: dsn {
    type: string
    sql: ${TABLE}."DSN" ;;
  }

  dimension: event_id {
    type: number
    # hidden: yes
    sql: ${TABLE}."EVENT_ID" ;;
  }

  dimension: host_comp {
    type: number
    sql: ${TABLE}."HOST_COMP" ;;
  }

  dimension: host_sold {
    type: number
    sql: ${TABLE}."HOST_SOLD" ;;
  }

  dimension: host_ticket_price {
    type: number
    sql: ${TABLE}."HOST_TICKET_PRICE" ;;
  }

  measure: count {
    type: count
    drill_fields: [detail*]
  }

  # ----- Sets of fields for drilling ------
  set: detail {
    fields: [
      event.primary_act_name,
      event.event_disp_name,
      event.promoter_name,
      event.secondary_act_name,
      event.event_id,
      event.event_name,
      event.major_cat_name,
      event.venue_name,
      event.minor_cat_name
    ]
  }
}
