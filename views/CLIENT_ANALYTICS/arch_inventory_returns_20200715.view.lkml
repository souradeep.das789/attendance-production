view: arch_inventory_returns_20200715 {
  sql_table_name: "CLIENT_ANALYTICS"."ARCH_INVENTORY_RETURNS_20200715"
    ;;

  dimension: archtics_comp {
    type: number
    sql: ${TABLE}."ARCHTICS_COMP" ;;
  }

  dimension: archtics_exceptions {
    type: number
    sql: ${TABLE}."ARCHTICS_EXCEPTIONS" ;;
  }

  dimension: archtics_hold {
    type: number
    sql: ${TABLE}."ARCHTICS_HOLD" ;;
  }

  dimension: archtics_inquiry {
    type: number
    sql: ${TABLE}."ARCHTICS_INQUIRY" ;;
  }

  dimension: archtics_kill {
    type: number
    sql: ${TABLE}."ARCHTICS_KILL" ;;
  }

  dimension: archtics_open {
    type: number
    sql: ${TABLE}."ARCHTICS_OPEN" ;;
  }

  dimension: archtics_sold {
    type: number
    sql: ${TABLE}."ARCHTICS_SOLD" ;;
  }

  dimension: archtics_ticket_price {
    type: number
    sql: ${TABLE}."ARCHTICS_TICKET_PRICE" ;;
  }

  dimension: base_price {
    type: number
    sql: ${TABLE}."BASE_PRICE" ;;
  }

  dimension: d_pc_ticket {
    type: string
    sql: ${TABLE}."D_PC_TICKET" ;;
  }

  dimension: discount_level {
    type: string
    sql: ${TABLE}."DISCOUNT_LEVEL" ;;
  }

  dimension: dist_sold {
    type: number
    sql: ${TABLE}."DIST_SOLD" ;;
  }

  dimension: dsn {
    type: string
    sql: ${TABLE}."DSN" ;;
  }

  dimension: eippc_pc_ticket {
    type: string
    sql: ${TABLE}."EIPPC_PC_TICKET" ;;
  }

  dimension: event_id {
    type: number
    # hidden: yes
    sql: ${TABLE}."EVENT_ID" ;;
  }

  dimension: event_name {
    type: string
    sql: ${TABLE}."EVENT_NAME" ;;
  }

  dimension: host_comp {
    type: number
    sql: ${TABLE}."HOST_COMP" ;;
  }

  dimension: host_hold {
    type: number
    sql: ${TABLE}."HOST_HOLD" ;;
  }

  dimension: host_inquiry {
    type: number
    sql: ${TABLE}."HOST_INQUIRY" ;;
  }

  dimension: host_kill {
    type: number
    sql: ${TABLE}."HOST_KILL" ;;
  }

  dimension: host_open {
    type: number
    sql: ${TABLE}."HOST_OPEN" ;;
  }

  dimension: host_sold {
    type: number
    sql: ${TABLE}."HOST_SOLD" ;;
  }

  dimension: host_ticket_price {
    type: number
    sql: ${TABLE}."HOST_TICKET_PRICE" ;;
  }

  dimension: num_seats {
    type: number
    sql: ${TABLE}."NUM_SEATS" ;;
  }

  dimension: pc_pc_ticket {
    type: string
    sql: ${TABLE}."PC_PC_TICKET" ;;
  }

  dimension: plan_event_id {
    type: number
    sql: ${TABLE}."PLAN_EVENT_ID" ;;
  }

  dimension: preprint {
    type: number
    sql: ${TABLE}."PREPRINT" ;;
  }

  dimension: price_code {
    type: string
    sql: ${TABLE}."PRICE_CODE" ;;
  }

  dimension: price_level {
    type: number
    sql: ${TABLE}."PRICE_LEVEL" ;;
  }

  dimension_group: return_ts {
    type: time
    timeframes: [
      raw,
      time,
      date,
      week,
      month,
      quarter,
      year
    ]
    sql: ${TABLE}."RETURN_TS" ;;
  }

  dimension: row_id {
    type: number
    sql: ${TABLE}."ROW_ID" ;;
  }

  dimension: seat_num {
    type: number
    sql: ${TABLE}."SEAT_NUM" ;;
  }

  dimension: section_id {
    type: number
    sql: ${TABLE}."SECTION_ID" ;;
  }

  dimension: stage {
    type: number
    sql: ${TABLE}."STAGE" ;;
  }

  dimension: t_pc_ticket {
    type: number
    sql: ${TABLE}."T_PC_TICKET" ;;
  }

  dimension: total_events {
    type: number
    sql: ${TABLE}."TOTAL_EVENTS" ;;
  }

  dimension_group: transaction_ts {
    type: time
    timeframes: [
      raw,
      time,
      date,
      week,
      month,
      quarter,
      year
    ]
    sql: ${TABLE}."TRANSACTION_TS" ;;
  }

  measure: count {
    type: count
    drill_fields: [detail*]
  }

  # ----- Sets of fields for drilling ------
  set: detail {
    fields: [
      event_name,
      event.primary_act_name,
      event.event_disp_name,
      event.promoter_name,
      event.secondary_act_name,
      event.event_id,
      event.event_name,
      event.major_cat_name,
      event.venue_name,
      event.minor_cat_name
    ]
  }
}
