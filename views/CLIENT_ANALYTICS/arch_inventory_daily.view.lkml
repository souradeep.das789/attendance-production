view: arch_inventory_daily {
  sql_table_name: "CLIENT_ANALYTICS"."ARCH_INVENTORY_DAILY"
    ;;

  dimension: archtics_comp {
    type: number
    sql: ${TABLE}."ARCHTICS_COMP" ;;
  }

  dimension: archtics_exceptions {
    type: number
    sql: ${TABLE}."ARCHTICS_EXCEPTIONS" ;;
  }

  dimension: archtics_hold {
    type: number
    sql: ${TABLE}."ARCHTICS_HOLD" ;;
  }

  dimension: archtics_inquiry {
    type: number
    sql: ${TABLE}."ARCHTICS_INQUIRY" ;;
  }

  dimension: archtics_kill {
    type: number
    sql: ${TABLE}."ARCHTICS_KILL" ;;
  }

  dimension: archtics_open {
    type: number
    sql: ${TABLE}."ARCHTICS_OPEN" ;;
  }

  dimension: archtics_sold {
    type: number
    sql: ${TABLE}."ARCHTICS_SOLD" ;;
  }

  dimension: archtics_ticket_price {
    type: number
    sql: ${TABLE}."ARCHTICS_TICKET_PRICE" ;;
  }

  dimension: dist_sold {
    type: number
    sql: ${TABLE}."DIST_SOLD" ;;
  }

  dimension: dsn {
    type: string
    sql: ${TABLE}."DSN" ;;
  }

  dimension: event_id {
    type: number
    # hidden: yes
    sql: ${TABLE}."EVENT_ID" ;;
  }

  dimension: host_comp {
    type: number
    sql: ${TABLE}."HOST_COMP" ;;
  }

  dimension: host_hold {
    type: number
    sql: ${TABLE}."HOST_HOLD" ;;
  }

  dimension: host_inquiry {
    type: number
    sql: ${TABLE}."HOST_INQUIRY" ;;
  }

  dimension: host_kill {
    type: number
    sql: ${TABLE}."HOST_KILL" ;;
  }

  dimension: host_open {
    type: number
    sql: ${TABLE}."HOST_OPEN" ;;
  }

  dimension: host_sold {
    type: number
    sql: ${TABLE}."HOST_SOLD" ;;
  }

  dimension: host_ticket_price {
    type: number
    sql: ${TABLE}."HOST_TICKET_PRICE" ;;
  }

  dimension: plan_event_id {
    type: number
    sql: ${TABLE}."PLAN_EVENT_ID" ;;
  }

  dimension: preprint {
    type: number
    sql: ${TABLE}."PREPRINT" ;;
  }

  dimension_group: txtime {
    type: time
    timeframes: [
      raw,
      date,
      week,
      month,
      quarter,
      year
    ]
    convert_tz: no
    datatype: date
    sql: ${TABLE}."TXTIME" ;;
  }

  measure: count {
    type: count
    drill_fields: [detail*]
  }

  # ----- Sets of fields for drilling ------
  set: detail {
    fields: [
      event.primary_act_name,
      event.event_disp_name,
      event.promoter_name,
      event.secondary_act_name,
      event.event_id,
      event.event_name,
      event.major_cat_name,
      event.venue_name,
      event.minor_cat_name
    ]
  }
}
