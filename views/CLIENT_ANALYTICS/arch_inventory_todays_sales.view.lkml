view: arch_inventory_todays_sales {
  sql_table_name: "CLIENT_ANALYTICS"."ARCH_INVENTORY_TODAYS_SALES"
    ;;

  dimension: arch_comp {
    type: number
    sql: ${TABLE}."ARCH_COMP" ;;
  }

  dimension: arch_dist_sold {
    type: number
    sql: ${TABLE}."ARCH_DIST_SOLD" ;;
  }

  dimension: arch_exceptions {
    type: number
    sql: ${TABLE}."ARCH_EXCEPTIONS" ;;
  }

  dimension: arch_hold {
    type: number
    sql: ${TABLE}."ARCH_HOLD" ;;
  }

  dimension: arch_inquiry {
    type: number
    sql: ${TABLE}."ARCH_INQUIRY" ;;
  }

  dimension: arch_kill {
    type: number
    sql: ${TABLE}."ARCH_KILL" ;;
  }

  dimension: arch_open {
    type: number
    sql: ${TABLE}."ARCH_OPEN" ;;
  }

  dimension: arch_preprint {
    type: number
    sql: ${TABLE}."ARCH_PREPRINT" ;;
  }

  dimension: arch_revenue {
    type: number
    sql: ${TABLE}."ARCH_REVENUE" ;;
  }

  dimension: arch_sold {
    type: number
    sql: ${TABLE}."ARCH_SOLD" ;;
  }

  dimension_group: collection_dt {
    type: time
    timeframes: [
      raw,
      date,
      week,
      month,
      quarter,
      year
    ]
    convert_tz: no
    datatype: date
    sql: ${TABLE}."COLLECTION_DT" ;;
  }

  dimension: dsn {
    type: string
    sql: ${TABLE}."DSN" ;;
  }

  dimension: event_id {
    type: number
    # hidden: yes
    sql: ${TABLE}."EVENT_ID" ;;
  }

  dimension: host_comp {
    type: number
    sql: ${TABLE}."HOST_COMP" ;;
  }

  dimension: host_hold {
    type: number
    sql: ${TABLE}."HOST_HOLD" ;;
  }

  dimension: host_inquiry {
    type: number
    sql: ${TABLE}."HOST_INQUIRY" ;;
  }

  dimension: host_kill {
    type: number
    sql: ${TABLE}."HOST_KILL" ;;
  }

  dimension: host_open {
    type: number
    sql: ${TABLE}."HOST_OPEN" ;;
  }

  dimension: host_revenue {
    type: number
    sql: ${TABLE}."HOST_REVENUE" ;;
  }

  dimension: host_sold {
    type: number
    sql: ${TABLE}."HOST_SOLD" ;;
  }

  dimension: total_comp {
    type: number
    sql: ${TABLE}."TOTAL_COMP" ;;
  }

  dimension: total_hold {
    type: number
    sql: ${TABLE}."TOTAL_HOLD" ;;
  }

  dimension: total_inquiry {
    type: number
    sql: ${TABLE}."TOTAL_INQUIRY" ;;
  }

  dimension: total_kills {
    type: number
    sql: ${TABLE}."TOTAL_KILLS" ;;
  }

  dimension: total_open {
    type: number
    sql: ${TABLE}."TOTAL_OPEN" ;;
  }

  dimension: total_revenue {
    type: number
    sql: ${TABLE}."TOTAL_REVENUE" ;;
  }

  dimension: total_sold {
    type: number
    sql: ${TABLE}."TOTAL_SOLD" ;;
  }

  measure: count {
    type: count
    drill_fields: [detail*]
  }

  # ----- Sets of fields for drilling ------
  set: detail {
    fields: [
      event.primary_act_name,
      event.event_disp_name,
      event.promoter_name,
      event.secondary_act_name,
      event.event_id,
      event.event_name,
      event.major_cat_name,
      event.venue_name,
      event.minor_cat_name
    ]
  }
}
