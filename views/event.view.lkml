view: event {
  sql_table_name: "PUBLIC"."EVENT"
    ;;
  drill_fields: [event_id]

  dimension: event_id {
    primary_key: yes
    type: string
    sql: ${TABLE}."EVENT_ID" ;;
  }

  dimension: access_priority_id {
    type: number
    sql: ${TABLE}."ACCESS_PRIORITY_ID" ;;
  }

  dimension: arch_event_code {
    type: string
    sql: ${TABLE}."ARCH_EVENT_CODE" ;;
  }

  dimension: arch_event_id {
    type: string
    sql: ${TABLE}."ARCH_EVENT_ID" ;;
  }

  dimension: cancelled_flg {
    type: string
    sql: ${TABLE}."CANCELLED_FLG" ;;
  }

  dimension: client_cd {
    type: string
    sql: ${TABLE}."CLIENT_CD" ;;
  }

  dimension: clientbp {
    type: number
    sql: ${TABLE}."CLIENTBP" ;;
  }

  dimension_group: create_dt {
    type: time
    timeframes: [
      raw,
      date,
      week,
      month,
      quarter,
      year
    ]
    convert_tz: no
    datatype: date
    sql: ${TABLE}."CREATE_DT" ;;
  }

  dimension: cur_ind {
    type: string
    sql: ${TABLE}."CUR_IND" ;;
  }

  dimension: digital_event_flg {
    type: string
    sql: ${TABLE}."DIGITAL_EVENT_FLG" ;;
  }

  dimension: event_code {
    type: string
    sql: ${TABLE}."EVENT_CODE" ;;
  }

  dimension_group: event_date_time_utc {
    type: time
    timeframes: [
      raw,
      time,
      date,
      week,
      month,
      quarter,
      year
    ]
    sql: ${TABLE}."EVENT_DATE_TIME_UTC" ;;
  }

  dimension: event_disp_name {
    type: string
    sql: ${TABLE}."EVENT_DISP_NAME" ;;
  }

  dimension_group: event_dt {
    type: time
    timeframes: [
      raw,
      date,
      week,
      month,
      quarter,
      year
    ]
    convert_tz: no
    datatype: date
    sql: ${TABLE}."EVENT_DT" ;;
  }

  dimension: event_id_hex {
    type: string
    sql: ${TABLE}."EVENT_ID_HEX" ;;
  }

  dimension: event_id_src_sys_cd {
    type: string
    sql: ${TABLE}."EVENT_ID_SRC_SYS_CD" ;;
  }

  dimension: event_name {
    type: string
    sql: ${TABLE}."EVENT_NAME" ;;
  }

  dimension: event_time {
    type: number
    sql: ${TABLE}."EVENT_TIME" ;;
  }

  dimension: event_type_cd {
    type: string
    sql: ${TABLE}."EVENT_TYPE_CD" ;;
  }

  dimension: event_type_desc {
    type: string
    sql: ${TABLE}."EVENT_TYPE_DESC" ;;
  }

  dimension: exch_active_flg {
    type: string
    sql: ${TABLE}."EXCH_ACTIVE_FLG" ;;
  }

  dimension: ism_enabled_flag {
    type: string
    sql: ${TABLE}."ISM_ENABLED_FLAG" ;;
  }

  dimension: major_cat_name {
    type: string
    sql: ${TABLE}."MAJOR_CAT_NAME" ;;
  }

  dimension: minor_cat_name {
    type: string
    sql: ${TABLE}."MINOR_CAT_NAME" ;;
  }

  dimension: nrep_music_service_flg {
    type: string
    sql: ${TABLE}."NREP_MUSIC_SERVICE_FLG" ;;
  }

  dimension_group: onsale_dt {
    type: time
    timeframes: [
      raw,
      date,
      week,
      month,
      quarter,
      year
    ]
    convert_tz: no
    datatype: date
    sql: ${TABLE}."ONSALE_DT" ;;
  }

  dimension_group: presale_dt {
    type: time
    timeframes: [
      raw,
      date,
      week,
      month,
      quarter,
      year
    ]
    convert_tz: no
    datatype: date
    sql: ${TABLE}."PRESALE_DT" ;;
  }

  dimension: primary_act_id {
    type: number
    sql: ${TABLE}."PRIMARY_ACT_ID" ;;
  }

  dimension: primary_act_name {
    type: string
    sql: ${TABLE}."PRIMARY_ACT_NAME" ;;
  }

  dimension: promoter_name {
    type: string
    sql: ${TABLE}."PROMOTER_NAME" ;;
  }

  dimension: row_sys_id {
    type: number
    sql: ${TABLE}."ROW_SYS_ID" ;;
  }

  dimension: secondary_act_id {
    type: number
    sql: ${TABLE}."SECONDARY_ACT_ID" ;;
  }

  dimension: secondary_act_name {
    type: string
    sql: ${TABLE}."SECONDARY_ACT_NAME" ;;
  }

  dimension: settle_cd {
    type: string
    sql: ${TABLE}."SETTLE_CD" ;;
  }

  dimension_group: td_insert_ts {
    type: time
    timeframes: [
      raw,
      time,
      date,
      week,
      month,
      quarter,
      year
    ]
    sql: ${TABLE}."TD_INSERT_TS" ;;
  }

  dimension_group: td_update_ts {
    type: time
    timeframes: [
      raw,
      time,
      date,
      week,
      month,
      quarter,
      year
    ]
    sql: ${TABLE}."TD_UPDATE_TS" ;;
  }

  dimension: ticket_text {
    type: string
    sql: ${TABLE}."TICKET_TEXT" ;;
  }

  dimension: timezn_cur_offset_val {
    type: number
    sql: ${TABLE}."TIMEZN_CUR_OFFSET_VAL" ;;
  }

  dimension: valid_ticketed_event_flag {
    type: string
    sql: ${TABLE}."VALID_TICKETED_EVENT_FLAG" ;;
  }

  dimension: ven_id {
    type: number
    sql: ${TABLE}."VEN_ID" ;;
  }

  dimension: ven_id_src_sys_cd {
    type: string
    sql: ${TABLE}."VEN_ID_SRC_SYS_CD" ;;
  }

  dimension: venue_city {
    type: string
    sql: ${TABLE}."VENUE_CITY" ;;
  }

  dimension: venue_name {
    type: string
    sql: ${TABLE}."VENUE_NAME" ;;
  }

  dimension: venue_state {
    type: string
    sql: ${TABLE}."VENUE_STATE" ;;
  }

  dimension: venue_zipcode {
    type: string
    sql: ${TABLE}."VENUE_ZIPCODE" ;;
  }

  dimension: zone_cd {
    type: number
    sql: ${TABLE}."ZONE_CD" ;;
  }

  measure: count {
    type: count
    drill_fields: [detail*]
  }

  # ----- Sets of fields for drilling ------
  set: detail {
    fields: [
      event_id,
      event_disp_name,
      secondary_act_name,
      promoter_name,
      major_cat_name,
      venue_name,
      event_name,
      minor_cat_name,
      primary_act_name
    ]
  }
}
