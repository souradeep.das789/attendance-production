view: bi_dim_event_all {
  sql_table_name: "PUBLIC"."BI_DIM_EVENT_ALL"
    ;;

  dimension: access_priority_id {
    type: number
    sql: ${TABLE}."ACCESS_PRIORITY_ID" ;;
  }

  dimension: atrcn_festival_flg {
    type: string
    sql: ${TABLE}."ATRCN_FESTIVAL_FLG" ;;
  }

  dimension: bi_dim_event_key {
    type: number
    sql: ${TABLE}."BI_DIM_EVENT_KEY" ;;
  }

  dimension: canada_event_flg {
    type: string
    sql: ${TABLE}."CANADA_EVENT_FLG" ;;
  }

  dimension: cancelled_flg {
    type: string
    sql: ${TABLE}."CANCELLED_FLG" ;;
  }

  dimension: chart_flg {
    type: string
    sql: ${TABLE}."CHART_FLG" ;;
  }

  dimension: client_support_rep_nm {
    type: string
    sql: ${TABLE}."CLIENT_SUPPORT_REP_NM" ;;
  }

  dimension: clientbp {
    type: number
    sql: ${TABLE}."CLIENTBP" ;;
  }

  dimension_group: create_dt {
    type: time
    timeframes: [
      raw,
      date,
      week,
      month,
      quarter,
      year
    ]
    convert_tz: no
    datatype: date
    sql: ${TABLE}."CREATE_DT" ;;
  }

  dimension: created_by {
    type: string
    sql: ${TABLE}."CREATED_BY" ;;
  }

  dimension: cur_ind {
    type: string
    sql: ${TABLE}."CUR_IND" ;;
  }

  dimension: db_econdid {
    type: number
    value_format_name: id
    sql: ${TABLE}."DB_ECONDID" ;;
  }

  dimension: eticket_flg {
    type: string
    sql: ${TABLE}."ETICKET_FLG" ;;
  }

  dimension: event_class_cd {
    type: string
    sql: ${TABLE}."EVENT_CLASS_CD" ;;
  }

  dimension: event_class_desc {
    type: string
    sql: ${TABLE}."EVENT_CLASS_DESC" ;;
  }

  dimension: event_code {
    type: string
    sql: ${TABLE}."EVENT_CODE" ;;
  }

  dimension_group: event_date {
    type: time
    timeframes: [
      raw,
      time,
      date,
      week,
      month,
      quarter,
      year
    ]
    sql: ${TABLE}."EVENT_DATE_TIME" ;;
  }

  dimension: event_disp_name {
    type: string
    sql: ${TABLE}."EVENT_DISP_NAME" ;;
  }

  dimension_group: event_dt {
    type: time
    timeframes: [
      raw,
      date,
      week,
      month,
      quarter,
      year
    ]
    convert_tz: no
    datatype: date
    sql: ${TABLE}."EVENT_DT" ;;
  }

  dimension: event_dttm_tbd_flg {
    type: string
    sql: ${TABLE}."EVENT_DTTM_TBD_FLG" ;;
  }

  dimension: event_id {
    type: string
    # hidden: yes
    sql: ${TABLE}."EVENT_ID" ;;
  }

  dimension: event_id_hex {
    type: string
    sql: ${TABLE}."EVENT_ID_HEX" ;;
  }

  dimension: event_id_src_sys_cd {
    type: string
    sql: ${TABLE}."EVENT_ID_SRC_SYS_CD" ;;
  }

  dimension: event_long_name {
    type: string
    sql: ${TABLE}."EVENT_LONG_NAME" ;;
  }

  dimension: event_name {
    type: string
    sql: ${TABLE}."EVENT_NAME" ;;
  }

  dimension: event_short_name {
    type: string
    sql: ${TABLE}."EVENT_SHORT_NAME" ;;
  }

  dimension: event_support_specialist_nm {
    type: string
    sql: ${TABLE}."EVENT_SUPPORT_SPECIALIST_NM" ;;
  }

  dimension: event_time {
    type: number
    sql: ${TABLE}."EVENT_TIME" ;;
  }

  dimension: event_type_cd {
    type: string
    sql: ${TABLE}."EVENT_TYPE_CD" ;;
  }

  dimension: event_type_desc {
    type: string
    sql: ${TABLE}."EVENT_TYPE_DESC" ;;
  }

  dimension: event_type_name {
    type: string
    sql: ${TABLE}."EVENT_TYPE_NAME" ;;
  }

  dimension_group: event_visible_dttm {
    type: time
    timeframes: [
      raw,
      time,
      date,
      week,
      month,
      quarter,
      year
    ]
    sql: ${TABLE}."EVENT_VISIBLE_DTTM" ;;
  }

  dimension_group: event_visible_utc_dttm {
    type: time
    timeframes: [
      raw,
      time,
      date,
      week,
      month,
      quarter,
      year
    ]
    sql: ${TABLE}."EVENT_VISIBLE_UTC_DTTM" ;;
  }

  dimension: exch_active_flg {
    type: string
    sql: ${TABLE}."EXCH_ACTIVE_FLG" ;;
  }

  dimension: exch_enabled {
    type: string
    sql: ${TABLE}."EXCH_ENABLED" ;;
  }

  dimension: fin_mkt_id {
    type: number
    sql: ${TABLE}."FIN_MKT_ID" ;;
  }

  dimension: fin_mkt_nm {
    type: string
    sql: ${TABLE}."FIN_MKT_NM" ;;
  }

  dimension: fin_mkt_region_cd {
    type: string
    sql: ${TABLE}."FIN_MKT_REGION_CD" ;;
  }

  dimension: geo_area_nm {
    type: string
    sql: ${TABLE}."GEO_AREA_NM" ;;
  }

  dimension: host_org_id {
    type: number
    sql: ${TABLE}."HOST_ORG_ID" ;;
  }

  dimension: host_sys_cd {
    type: string
    sql: ${TABLE}."HOST_SYS_CD" ;;
  }

  dimension: ism_enabled_flag {
    type: string
    sql: ${TABLE}."ISM_ENABLED_FLAG" ;;
  }

  dimension: ism_price_slider_flg {
    type: string
    sql: ${TABLE}."ISM_PRICE_SLIDER_FLG" ;;
  }

  dimension: jetson_event_id {
    type: string
    sql: ${TABLE}."JETSON_EVENT_ID" ;;
  }

  dimension_group: la_event_dt {
    type: time
    timeframes: [
      raw,
      date,
      week,
      month,
      quarter,
      year
    ]
    convert_tz: no
    datatype: date
    sql: ${TABLE}."LA_EVENT_DT" ;;
  }

  dimension: la_event_type_cat {
    type: string
    sql: ${TABLE}."LA_EVENT_TYPE_CAT" ;;
  }

  dimension_group: la_onsale_dt {
    type: time
    timeframes: [
      raw,
      date,
      week,
      month,
      quarter,
      year
    ]
    convert_tz: no
    datatype: date
    sql: ${TABLE}."LA_ONSALE_DT" ;;
  }

  dimension_group: la_presale_dt {
    type: time
    timeframes: [
      raw,
      date,
      week,
      month,
      quarter,
      year
    ]
    convert_tz: no
    datatype: date
    sql: ${TABLE}."LA_PRESALE_DT" ;;
  }

  dimension: la_valid_tkt_event_flg {
    type: string
    sql: ${TABLE}."LA_VALID_TKT_EVENT_FLG" ;;
  }

  dimension: ln_flag {
    type: string
    sql: ${TABLE}."LN_FLAG" ;;
  }

  dimension: major_cat_id {
    type: number
    sql: ${TABLE}."MAJOR_CAT_ID" ;;
  }

  dimension: major_cat_name {
    type: string
    sql: ${TABLE}."MAJOR_CAT_NAME" ;;
  }

  dimension: minor_cat_id {
    type: number
    sql: ${TABLE}."MINOR_CAT_ID" ;;
  }

  dimension: minor_cat_name {
    type: string
    sql: ${TABLE}."MINOR_CAT_NAME" ;;
  }

  dimension: mobile_tkt_flg {
    type: string
    sql: ${TABLE}."MOBILE_TKT_FLG" ;;
  }

  dimension: multiple_event_dt_flg {
    type: string
    sql: ${TABLE}."MULTIPLE_EVENT_DT_FLG" ;;
  }

  dimension: nrep_music_service_flg {
    type: string
    sql: ${TABLE}."NREP_MUSIC_SERVICE_FLG" ;;
  }

  dimension_group: offset_ts {
    type: time
    timeframes: [
      raw,
      time,
      date,
      week,
      month,
      quarter,
      year
    ]
    sql: ${TABLE}."OFFSET_TS" ;;
  }

  dimension_group: onsale_dt {
    type: time
    timeframes: [
      raw,
      date,
      week,
      month,
      quarter,
      year
    ]
    convert_tz: no
    datatype: date
    sql: ${TABLE}."ONSALE_DT" ;;
  }

  dimension: onsale_flg {
    type: string
    sql: ${TABLE}."ONSALE_FLG" ;;
  }

  dimension: onsale_rank_flg {
    type: string
    sql: ${TABLE}."ONSALE_RANK_FLG" ;;
  }

  dimension: paperless_tkt_flg {
    type: string
    sql: ${TABLE}."PAPERLESS_TKT_FLG" ;;
  }

  dimension_group: phn_sale_end_dttm {
    type: time
    timeframes: [
      raw,
      time,
      date,
      week,
      month,
      quarter,
      year
    ]
    sql: ${TABLE}."PHN_SALE_END_DTTM" ;;
  }

  dimension_group: phn_sale_end_utc_dttm {
    type: time
    timeframes: [
      raw,
      time,
      date,
      week,
      month,
      quarter,
      year
    ]
    sql: ${TABLE}."PHN_SALE_END_UTC_DTTM" ;;
  }

  dimension: postponed_flg {
    type: string
    sql: ${TABLE}."POSTPONED_FLG" ;;
  }

  dimension_group: presale_dt {
    type: time
    timeframes: [
      raw,
      date,
      week,
      month,
      quarter,
      year
    ]
    convert_tz: no
    datatype: date
    sql: ${TABLE}."PRESALE_DT" ;;
  }

  dimension_group: presale_off_dt {
    type: time
    timeframes: [
      raw,
      date,
      week,
      month,
      quarter,
      year
    ]
    convert_tz: no
    datatype: date
    sql: ${TABLE}."PRESALE_OFF_DT" ;;
  }

  dimension: primary_act_id {
    type: number
    sql: ${TABLE}."PRIMARY_ACT_ID" ;;
  }

  dimension: primary_act_name {
    type: string
    sql: ${TABLE}."PRIMARY_ACT_NAME" ;;
  }

  dimension: promoter_host_id {
    type: string
    sql: ${TABLE}."PROMOTER_HOST_ID" ;;
  }

  dimension: promoter_host_id2 {
    type: string
    sql: ${TABLE}."PROMOTER_HOST_ID2" ;;
  }

  dimension: promoter_name {
    type: string
    sql: ${TABLE}."PROMOTER_NAME" ;;
  }

  dimension: promoter_name2 {
    type: string
    sql: ${TABLE}."PROMOTER_NAME2" ;;
  }

  dimension: public_onsale_asap_flg {
    type: string
    sql: ${TABLE}."PUBLIC_ONSALE_ASAP_FLG" ;;
  }

  dimension: public_onsale_tba_flg {
    type: string
    sql: ${TABLE}."PUBLIC_ONSALE_TBA_FLG" ;;
  }

  dimension_group: resale_activation_dt {
    type: time
    timeframes: [
      raw,
      date,
      week,
      month,
      quarter,
      year
    ]
    convert_tz: no
    datatype: date
    sql: ${TABLE}."RESALE_ACTIVATION_DT" ;;
  }

  dimension: resale_cur_enabled_flg {
    type: string
    sql: ${TABLE}."RESALE_CUR_ENABLED_FLG" ;;
  }

  dimension: resale_enabled_flg {
    type: string
    sql: ${TABLE}."RESALE_ENABLED_FLG" ;;
  }

  dimension: rescheduled_flg {
    type: string
    sql: ${TABLE}."RESCHEDULED_FLG" ;;
  }

  dimension: roll_event_flg {
    type: string
    sql: ${TABLE}."ROLL_EVENT_FLG" ;;
  }

  dimension: secondary_act_id {
    type: number
    sql: ${TABLE}."SECONDARY_ACT_ID" ;;
  }

  dimension: secondary_act_name {
    type: string
    sql: ${TABLE}."SECONDARY_ACT_NAME" ;;
  }

  dimension: secure_entry_flg {
    type: string
    sql: ${TABLE}."SECURE_ENTRY_FLG" ;;
  }

  dimension: series_master_flg {
    type: string
    sql: ${TABLE}."SERIES_MASTER_FLG" ;;
  }

  dimension: series_member_flg {
    type: string
    sql: ${TABLE}."SERIES_MEMBER_FLG" ;;
  }

  dimension: settle_cd {
    type: string
    sql: ${TABLE}."SETTLE_CD" ;;
  }

  dimension: splitid {
    type: number
    value_format_name: id
    sql: ${TABLE}."SPLITID" ;;
  }

  dimension_group: td_insert_ts {
    type: time
    timeframes: [
      raw,
      time,
      date,
      week,
      month,
      quarter,
      year
    ]
    sql: ${TABLE}."TD_INSERT_TS" ;;
  }

  dimension_group: td_update_ts {
    type: time
    timeframes: [
      raw,
      time,
      date,
      week,
      month,
      quarter,
      year
    ]
    sql: ${TABLE}."TD_UPDATE_TS" ;;
  }

  dimension: ticket_text {
    type: string
    sql: ${TABLE}."TICKET_TEXT" ;;
  }

  dimension: ticketfast_delay_flg {
    type: string
    sql: ${TABLE}."TICKETFAST_DELAY_FLG" ;;
  }

  dimension: ticketfast_ena_flg {
    type: string
    sql: ${TABLE}."TICKETFAST_ENA_FLG" ;;
  }

  dimension: ticketfast_flg {
    type: string
    sql: ${TABLE}."TICKETFAST_FLG" ;;
  }

  dimension: timezn_cur_offset_val {
    type: number
    sql: ${TABLE}."TIMEZN_CUR_OFFSET_VAL" ;;
  }

  dimension: timezn_nm {
    type: string
    sql: ${TABLE}."TIMEZN_NM" ;;
  }

  dimension: tkt_ready_minute_offset_num {
    type: number
    sql: ${TABLE}."TKT_READY_MINUTE_OFFSET_NUM" ;;
  }

  dimension: tkt_ready_mobile_minute_offset_num {
    type: number
    sql: ${TABLE}."TKT_READY_MOBILE_MINUTE_OFFSET_NUM" ;;
  }

  dimension: tm_primary_attraction_id {
    type: number
    sql: ${TABLE}."TM_PRIMARY_ATTRACTION_ID" ;;
  }

  dimension: tm_secondary_attraction_id {
    type: number
    sql: ${TABLE}."TM_SECONDARY_ATTRACTION_ID" ;;
  }

  dimension: tnow_event_flg {
    type: string
    sql: ${TABLE}."TNOW_EVENT_FLG" ;;
  }

  dimension: total_cap_qty {
    type: number
    sql: ${TABLE}."TOTAL_CAP_QTY" ;;
  }

  dimension_group: updated_ts {
    type: time
    timeframes: [
      raw,
      time,
      date,
      week,
      month,
      quarter,
      year
    ]
    sql: CAST(${TABLE}."UPDATED_TS" AS TIMESTAMP_NTZ) ;;
  }

  dimension: us_event_flg {
    type: string
    sql: ${TABLE}."US_EVENT_FLG" ;;
  }

  dimension: valid_ticketed_event_flag {
    type: string
    sql: ${TABLE}."VALID_TICKETED_EVENT_FLAG" ;;
  }

  dimension: ven_id {
    type: number
    sql: ${TABLE}."VEN_ID" ;;
  }

  dimension: ven_id_src_sys_cd {
    type: string
    sql: ${TABLE}."VEN_ID_SRC_SYS_CD" ;;
  }

  dimension: zone_cd {
    type: number
    sql: ${TABLE}."ZONE_CD" ;;
  }

  measure: count {
    type: count
    drill_fields: [detail*]
  }

  # ----- Sets of fields for drilling ------
  set: detail {
    fields: [
      primary_act_name,
      event_long_name,
      event_disp_name,
      minor_cat_name,
      promoter_name,
      major_cat_name,
      event_name,
      secondary_act_name,
      event_short_name,
      event_type_name,
      event.primary_act_name,
      event.event_disp_name,
      event.promoter_name,
      event.secondary_act_name,
      event.event_id,
      event.event_name,
      event.major_cat_name,
      event.venue_name,
      event.minor_cat_name
    ]
  }
}
