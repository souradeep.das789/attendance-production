view: ga_mp_hits {
  sql_table_name: "PUBLIC"."GA_MP_HITS"
    ;;

  dimension: adwords_ad_group_id {
    type: string
    sql: ${TABLE}."ADWORDS_AD_GROUP_ID" ;;
  }

  dimension: adwords_click_id {
    type: string
    sql: ${TABLE}."ADWORDS_CLICK_ID" ;;
  }

  dimension: adwords_customer_id {
    type: string
    sql: ${TABLE}."ADWORDS_CUSTOMER_ID" ;;
  }

  dimension: campaign {
    type: string
    sql: ${TABLE}."CAMPAIGN" ;;
  }

  dimension: cd_artist_id {
    type: string
    sql: ${TABLE}."CD_ARTIST_ID" ;;
  }

  dimension: cd_came_from_code {
    type: string
    sql: ${TABLE}."CD_CAME_FROM_CODE" ;;
  }

  dimension: cd_edp_reserve_type {
    type: string
    sql: ${TABLE}."CD_EDP_RESERVE_TYPE" ;;
  }

  dimension: cd_event_id {
    type: string
    sql: ${TABLE}."CD_EVENT_ID" ;;
  }

  dimension: cd_member_id {
    type: string
    sql: ${TABLE}."CD_MEMBER_ID" ;;
  }

  dimension: cd_platinum_flag {
    type: string
    sql: ${TABLE}."CD_PLATINUM_FLAG" ;;
  }

  dimension: cd_referral_hostname {
    type: string
    sql: ${TABLE}."CD_REFERRAL_HOSTNAME" ;;
  }

  dimension: cd_resale_flag {
    type: string
    sql: ${TABLE}."CD_RESALE_FLAG" ;;
  }

  dimension: cd_tnow_flag {
    type: string
    sql: ${TABLE}."CD_TNOW_FLAG" ;;
  }

  dimension: cd_tracking_code {
    type: string
    sql: ${TABLE}."CD_TRACKING_CODE" ;;
  }

  dimension: channel_grouping {
    type: string
    sql: ${TABLE}."CHANNEL_GROUPING" ;;
  }

  dimension: device_category {
    type: string
    sql: ${TABLE}."DEVICE_CATEGORY" ;;
  }

  dimension: device_info {
    type: string
    sql: ${TABLE}."DEVICE_INFO" ;;
  }

  dimension: device_operating_system {
    type: string
    sql: ${TABLE}."DEVICE_OPERATING_SYSTEM" ;;
  }

  dimension: ecommerce_action_type {
    type: string
    sql: ${TABLE}."ECOMMERCE_ACTION_TYPE" ;;
  }

  dimension: ecommerce_checkout_step {
    type: string
    sql: ${TABLE}."ECOMMERCE_CHECKOUT_STEP" ;;
  }

  dimension: full_visitor_id {
    type: string
    sql: ${TABLE}."FULL_VISITOR_ID" ;;
  }

  dimension: geo_city {
    type: string
    sql: ${TABLE}."GEO_CITY" ;;
  }

  dimension: geo_city_id {
    type: string
    sql: ${TABLE}."GEO_CITY_ID" ;;
  }

  dimension: geo_country {
    type: string
    sql: ${TABLE}."GEO_COUNTRY" ;;
  }

  dimension: geo_latitude {
    type: string
    sql: ${TABLE}."GEO_LATITUDE" ;;
  }

  dimension: geo_location {
    type: string
    sql: ${TABLE}."GEO_LOCATION" ;;
  }

  dimension: geo_longitude {
    type: string
    sql: ${TABLE}."GEO_LONGITUDE" ;;
  }

  dimension: geo_metro {
    type: string
    sql: ${TABLE}."GEO_METRO" ;;
  }

  dimension: geo_region {
    type: string
    sql: ${TABLE}."GEO_REGION" ;;
  }

  dimension: hit_date_and_time {
    type: string
    sql: ${TABLE}."HIT_DATE_AND_TIME" ;;
  }

  dimension: hit_event_action {
    type: string
    sql: ${TABLE}."HIT_EVENT_ACTION" ;;
  }

  dimension: hit_event_category {
    type: string
    sql: ${TABLE}."HIT_EVENT_CATEGORY" ;;
  }

  dimension: hit_event_label {
    type: string
    sql: ${TABLE}."HIT_EVENT_LABEL" ;;
  }

  dimension: hit_event_value {
    type: string
    sql: ${TABLE}."HIT_EVENT_VALUE" ;;
  }

  dimension: hit_id {
    type: string
    sql: ${TABLE}."HIT_ID" ;;
  }

  dimension: hit_number {
    type: string
    sql: ${TABLE}."HIT_NUMBER" ;;
  }

  dimension: hit_page_path {
    type: string
    sql: ${TABLE}."HIT_PAGE_PATH" ;;
  }

  dimension: hit_page_title {
    type: string
    sql: ${TABLE}."HIT_PAGE_TITLE" ;;
  }

  dimension: hit_time {
    type: string
    sql: ${TABLE}."HIT_TIME" ;;
  }

  dimension: hit_type {
    type: string
    sql: ${TABLE}."HIT_TYPE" ;;
  }

  dimension: hostname {
    type: string
    sql: ${TABLE}."HOSTNAME" ;;
  }

  dimension: keyword {
    type: string
    sql: ${TABLE}."KEYWORD" ;;
  }

  dimension: local_product_revenue {
    type: string
    sql: ${TABLE}."LOCAL_PRODUCT_REVENUE" ;;
  }

  dimension: local_transaction_revenue {
    type: string
    sql: ${TABLE}."LOCAL_TRANSACTION_REVENUE" ;;
  }

  dimension: medium {
    type: string
    sql: ${TABLE}."MEDIUM" ;;
  }

  dimension: product_price {
    type: string
    sql: ${TABLE}."PRODUCT_PRICE" ;;
  }

  dimension: product_quantity {
    type: string
    sql: ${TABLE}."PRODUCT_QUANTITY" ;;
  }

  dimension: product_revenue {
    type: string
    sql: ${TABLE}."PRODUCT_REVENUE" ;;
  }

  dimension: product_sku_event_id {
    type: string
    sql: ${TABLE}."PRODUCT_SKU_EVENT_ID" ;;
  }

  dimension: product_variant_counter {
    type: string
    sql: ${TABLE}."PRODUCT_VARIANT_COUNTER" ;;
  }

  dimension: product_variant_ticket_id {
    type: string
    sql: ${TABLE}."PRODUCT_VARIANT_TICKET_ID" ;;
  }

  dimension: record_id {
    type: string
    sql: ${TABLE}."RECORD_ID" ;;
  }

  dimension: referral_path {
    type: string
    sql: ${TABLE}."REFERRAL_PATH" ;;
  }

  dimension: session_date_and_time {
    type: string
    sql: ${TABLE}."SESSION_DATE_AND_TIME" ;;
  }

  dimension: session_entrance_page {
    type: string
    sql: ${TABLE}."SESSION_ENTRANCE_PAGE" ;;
  }

  dimension: session_entrance_page_title {
    type: string
    sql: ${TABLE}."SESSION_ENTRANCE_PAGE_TITLE" ;;
  }

  dimension: session_exit_page {
    type: string
    sql: ${TABLE}."SESSION_EXIT_PAGE" ;;
  }

  dimension: session_id {
    type: string
    sql: ${TABLE}."SESSION_ID" ;;
  }

  dimension: session_number {
    type: string
    sql: ${TABLE}."SESSION_NUMBER" ;;
  }

  dimension: session_start_time {
    type: string
    sql: ${TABLE}."SESSION_START_TIME" ;;
  }

  dimension: source {
    type: string
    sql: ${TABLE}."SOURCE" ;;
  }

  dimension: transaction_currency_code {
    type: string
    sql: ${TABLE}."TRANSACTION_CURRENCY_CODE" ;;
  }

  dimension: transaction_id {
    type: string
    sql: ${TABLE}."TRANSACTION_ID" ;;
  }

  dimension: transaction_revenue {
    type: string
    sql: ${TABLE}."TRANSACTION_REVENUE" ;;
  }

  dimension: user_id {
    type: string
    sql: ${TABLE}."USER_ID" ;;
  }

  dimension: visit_id {
    type: string
    sql: ${TABLE}."VISIT_ID" ;;
  }

  dimension_group: visit_start_dt {
    type: time
    timeframes: [
      raw,
      date,
      week,
      month,
      quarter,
      year
    ]
    convert_tz: no
    datatype: date
    sql: ${TABLE}."VISIT_START_DT" ;;
  }

  measure: count {
    type: count
    drill_fields: [hostname, cd_referral_hostname]
  }
}
