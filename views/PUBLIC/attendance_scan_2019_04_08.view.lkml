view: attendance_scan_2019_04_08 {
  sql_table_name: "PUBLIC"."ATTENDANCE_SCAN_2019_04_08"
    ;;

  dimension: access_mgr_sys_id {
    type: string
    sql: ${TABLE}."ACCESS_MGR_SYS_ID" ;;
  }

  dimension: archtics_dsn_cd {
    type: string
    sql: ${TABLE}."ARCHTICS_DSN_CD" ;;
  }

  dimension: attendance_scan_id {
    type: number
    # hidden: yes
    sql: ${TABLE}."ATTENDANCE_SCAN_ID" ;;
  }

  dimension: attendance_src_sys_cd {
    type: string
    sql: ${TABLE}."ATTENDANCE_SRC_SYS_CD" ;;
  }

  dimension: barcode {
    type: string
    sql: ${TABLE}."BARCODE" ;;
  }

  dimension: base_tkt_type_id {
    type: string
    sql: ${TABLE}."BASE_TKT_TYPE_ID" ;;
  }

  dimension: client_nm {
    type: string
    sql: ${TABLE}."CLIENT_NM" ;;
  }

  dimension: cust_id {
    type: string
    sql: ${TABLE}."CUST_ID" ;;
  }

  dimension: delivery_channel_cd {
    type: string
    sql: ${TABLE}."DELIVERY_CHANNEL_CD" ;;
  }

  dimension: disc_ven_id {
    type: string
    sql: ${TABLE}."DISC_VEN_ID" ;;
  }

  dimension: event_cd {
    type: string
    sql: ${TABLE}."EVENT_CD" ;;
  }

  dimension_group: event_dt {
    type: time
    timeframes: [
      raw,
      date,
      week,
      month,
      quarter,
      year
    ]
    convert_tz: no
    datatype: date
    sql: ${TABLE}."EVENT_DT" ;;
  }

  dimension: event_id {
    type: string
    # hidden: yes
    sql: ${TABLE}."EVENT_ID" ;;
  }

  dimension: event_id_src_sys_cd {
    type: string
    sql: ${TABLE}."EVENT_ID_SRC_SYS_CD" ;;
  }

  dimension: fan_device_os_nm {
    type: string
    sql: ${TABLE}."FAN_DEVICE_OS_NM" ;;
  }

  dimension: gate_id {
    type: string
    sql: ${TABLE}."GATE_ID" ;;
  }

  dimension: gate_nm {
    type: string
    sql: ${TABLE}."GATE_NM" ;;
  }

  dimension_group: insert_ts {
    type: time
    timeframes: [
      raw,
      time,
      date,
      week,
      month,
      quarter,
      year
    ]
    sql: ${TABLE}."INSERT_TS" ;;
  }

  dimension_group: msg_upload_dttm {
    type: time
    timeframes: [
      raw,
      time,
      date,
      week,
      month,
      quarter,
      year
    ]
    sql: ${TABLE}."MSG_UPLOAD_DTTM" ;;
  }

  dimension_group: ods_ts {
    type: time
    timeframes: [
      raw,
      time,
      date,
      week,
      month,
      quarter,
      year
    ]
    sql: ${TABLE}."ODS_TS" ;;
  }

  dimension: print_cnt {
    type: number
    sql: ${TABLE}."PRINT_CNT" ;;
  }

  dimension: reject_cd {
    type: string
    sql: ${TABLE}."REJECT_CD" ;;
  }

  dimension: reject_reason_nm {
    type: string
    sql: ${TABLE}."REJECT_REASON_NM" ;;
  }

  dimension: row_num {
    type: string
    sql: ${TABLE}."ROW_NUM" ;;
  }

  dimension_group: scan_dt_utc {
    type: time
    timeframes: [
      raw,
      date,
      week,
      month,
      quarter,
      year
    ]
    convert_tz: no
    datatype: date
    sql: ${TABLE}."SCAN_DT_UTC" ;;
  }

  dimension_group: scan_dttm_utc {
    type: time
    timeframes: [
      raw,
      time,
      date,
      week,
      month,
      quarter,
      year
    ]
    sql: ${TABLE}."SCAN_DTTM_UTC" ;;
  }

  dimension: scan_result_cd {
    type: string
    sql: ${TABLE}."SCAN_RESULT_CD" ;;
  }

  dimension: scan_seq_num {
    type: number
    sql: ${TABLE}."SCAN_SEQ_NUM" ;;
  }

  dimension: scan_token_id {
    type: string
    sql: ${TABLE}."SCAN_TOKEN_ID" ;;
  }

  dimension: scanner_device_id {
    type: string
    sql: ${TABLE}."SCANNER_DEVICE_ID" ;;
  }

  dimension: scanner_device_nm {
    type: string
    sql: ${TABLE}."SCANNER_DEVICE_NM" ;;
  }

  dimension: scanner_software_version_num {
    type: string
    sql: ${TABLE}."SCANNER_SOFTWARE_VERSION_NUM" ;;
  }

  dimension: seat_cd {
    type: string
    sql: ${TABLE}."SEAT_CD" ;;
  }

  dimension: seat_num {
    type: string
    sql: ${TABLE}."SEAT_NUM" ;;
  }

  dimension: sect_nm {
    type: string
    sql: ${TABLE}."SECT_NM" ;;
  }

  dimension: ticket_id {
    type: string
    sql: ${TABLE}."TICKET_ID" ;;
  }

  dimension: tkt_status_cd {
    type: string
    sql: ${TABLE}."TKT_STATUS_CD" ;;
  }

  dimension_group: update_ts {
    type: time
    timeframes: [
      raw,
      time,
      date,
      week,
      month,
      quarter,
      year
    ]
    sql: ${TABLE}."UPDATE_TS" ;;
  }

  measure: count {
    type: count
    drill_fields: [detail*]
  }

  # ----- Sets of fields for drilling ------
  set: detail {
    fields: [
      event.primary_act_name,
      event.event_disp_name,
      event.promoter_name,
      event.secondary_act_name,
      event.event_id,
      event.event_name,
      event.major_cat_name,
      event.venue_name,
      event.minor_cat_name,
      attendance_scan.attendance_scan_id
    ]
  }
}
