view: tmp_attendance_event_xref_shared_single_client {
  sql_table_name: "PUBLIC"."TMP_ATTENDANCE_EVENT_XREF_SHARED_SINGLE_CLIENT"
    ;;

  dimension: arch_event_id {
    type: string
    sql: ${TABLE}."ARCH_EVENT_ID" ;;
  }

  dimension: client_cd {
    type: string
    sql: ${TABLE}."CLIENT_CD" ;;
  }

  dimension: event_id_hex {
    type: string
    sql: ${TABLE}."EVENT_ID_HEX" ;;
  }

  measure: count {
    type: count
    drill_fields: []
  }
}
