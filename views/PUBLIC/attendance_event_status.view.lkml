view: attendance_event_status {
  sql_table_name: "PUBLIC"."ATTENDANCE_EVENT_STATUS"
    ;;

  dimension: arch_event_id {
    type: string
    sql: ${TABLE}."ARCH_EVENT_ID" ;;
  }

  dimension: attendance_src_sys_cd {
    type: string
    sql: ${TABLE}."ATTENDANCE_SRC_SYS_CD" ;;
  }

  dimension: client_cd {
    type: string
    sql: ${TABLE}."CLIENT_CD" ;;
  }

  dimension: event_cd {
    type: string
    sql: ${TABLE}."EVENT_CD" ;;
  }

  dimension: event_id_hex {
    type: string
    sql: ${TABLE}."EVENT_ID_HEX" ;;
  }

  dimension: ghost_ind {
    type: yesno
    sql: ${TABLE}."GHOST_IND" ;;
  }

  dimension_group: insert_ts {
    type: time
    timeframes: [
      raw,
      time,
      date,
      week,
      month,
      quarter,
      year
    ]
    sql: ${TABLE}."INSERT_TS" ;;
  }

  dimension: primary_ingress_ind {
    type: yesno
    sql: ${TABLE}."PRIMARY_INGRESS_IND" ;;
  }

  dimension_group: update_ts {
    type: time
    timeframes: [
      raw,
      time,
      date,
      week,
      month,
      quarter,
      year
    ]
    sql: ${TABLE}."UPDATE_TS" ;;
  }

  measure: count {
    type: count
    drill_fields: []
  }
}
