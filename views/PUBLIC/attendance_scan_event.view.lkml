view: attendance_scan_event {
  sql_table_name: "PUBLIC"."ATTENDANCE_SCAN_EVENT"
    ;;

  dimension: arch_event_id {
    type: string
    sql: ${TABLE}."ARCH_EVENT_ID" ;;
  }

  dimension: client_cd {
    type: string
    sql: ${TABLE}."CLIENT_CD" ;;
  }

  dimension: event_id_hex {
    type: string
    sql: ${TABLE}."EVENT_ID_HEX" ;;
  }

  dimension: scan_event_id {
    type: string
    sql: ${TABLE}."SCAN_EVENT_ID" ;;
  }

  measure: count {
    type: count
    drill_fields: []
  }
}
