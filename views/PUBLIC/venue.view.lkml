view: venue {
  sql_table_name: "PUBLIC"."VENUE"
    ;;
  drill_fields: [host_venue_id]

  dimension: host_venue_id {
    primary_key: yes
    type: string
    sql: ${TABLE}."HOST_VENUE_ID" ;;
  }

  dimension: archtics_version_nm {
    type: string
    sql: ${TABLE}."ARCHTICS_VERSION_NM" ;;
  }

  dimension: cdd_nm {
    type: string
    sql: ${TABLE}."CDD_NM" ;;
  }

  dimension: client_nm {
    type: string
    sql: ${TABLE}."CLIENT_NM" ;;
  }

  dimension: csm_nm {
    type: string
    sql: ${TABLE}."CSM_NM" ;;
  }

  dimension: ctry_cd {
    type: string
    sql: ${TABLE}."CTRY_CD" ;;
  }

  dimension: ctry_nm {
    type: string
    sql: ${TABLE}."CTRY_NM" ;;
  }

  dimension: eventdb_src_loc_id {
    type: string
    sql: ${TABLE}."EVENTDB_SRC_LOC_ID" ;;
  }

  dimension: host_sys_cd {
    type: string
    sql: ${TABLE}."HOST_SYS_CD" ;;
  }

  dimension: lnoo_venue_flg {
    type: string
    sql: ${TABLE}."LNOO_VENUE_FLG" ;;
  }

  dimension: loc_cat_id {
    type: string
    sql: ${TABLE}."LOC_CAT_ID" ;;
  }

  dimension: loc_cat_nm {
    type: string
    sql: ${TABLE}."LOC_CAT_NM" ;;
  }

  dimension: loc_cat_src_sys_cd {
    type: string
    sql: ${TABLE}."LOC_CAT_SRC_SYS_CD" ;;
  }

  dimension: loc_type_id {
    type: string
    sql: ${TABLE}."LOC_TYPE_ID" ;;
  }

  dimension: loc_type_nm {
    type: string
    sql: ${TABLE}."LOC_TYPE_NM" ;;
  }

  dimension: loc_type_src_sys_cd {
    type: string
    sql: ${TABLE}."LOC_TYPE_SRC_SYS_CD" ;;
  }

  dimension: ms_coordinator_nm {
    type: string
    sql: ${TABLE}."MS_COORDINATOR_NM" ;;
  }

  dimension: ms_director_nm {
    type: string
    sql: ${TABLE}."MS_DIRECTOR_NM" ;;
  }

  dimension: mss_nm {
    type: string
    sql: ${TABLE}."MSS_NM" ;;
  }

  dimension_group: offset_ts {
    type: time
    timeframes: [
      raw,
      time,
      date,
      week,
      month,
      quarter,
      year
    ]
    sql: ${TABLE}."OFFSET_TS" ;;
  }

  dimension: org_type_nm {
    type: string
    sql: ${TABLE}."ORG_TYPE_NM" ;;
  }

  dimension: prev_archtics_version_nm {
    type: string
    sql: ${TABLE}."PREV_ARCHTICS_VERSION_NM" ;;
  }

  dimension: sap_busn_partner_id {
    type: number
    sql: ${TABLE}."SAP_BUSN_PARTNER_ID" ;;
  }

  dimension: splitid {
    type: number
    value_format_name: id
    sql: ${TABLE}."SPLITID" ;;
  }

  dimension_group: td_insert_ts {
    type: time
    timeframes: [
      raw,
      time,
      date,
      week,
      month,
      quarter,
      year
    ]
    sql: ${TABLE}."TD_INSERT_TS" ;;
  }

  dimension_group: td_update_ts {
    type: time
    timeframes: [
      raw,
      time,
      date,
      week,
      month,
      quarter,
      year
    ]
    sql: ${TABLE}."TD_UPDATE_TS" ;;
  }

  dimension: tmplus_status_nm {
    type: string
    sql: ${TABLE}."TMPLUS_STATUS_NM" ;;
  }

  dimension_group: updated_ts {
    type: time
    timeframes: [
      raw,
      time,
      date,
      week,
      month,
      quarter,
      year
    ]
    sql: CAST(${TABLE}."UPDATED_TS" AS TIMESTAMP_NTZ) ;;
  }

  dimension: ven_id_src_sys_cd {
    type: string
    sql: ${TABLE}."VEN_ID_SRC_SYS_CD" ;;
  }

  dimension: ven_latitude {
    type: number
    sql: ${TABLE}."VEN_LATITUDE" ;;
  }

  dimension: ven_longitude {
    type: number
    sql: ${TABLE}."VEN_LONGITUDE" ;;
  }

  dimension: ven_mkt_cd {
    type: string
    sql: ${TABLE}."VEN_MKT_CD" ;;
  }

  dimension: venue_city {
    type: string
    sql: ${TABLE}."VENUE_CITY" ;;
  }

  dimension: venue_dma_id {
    type: number
    sql: ${TABLE}."VENUE_DMA_ID" ;;
  }

  dimension: venue_dma_name {
    type: string
    sql: ${TABLE}."VENUE_DMA_NAME" ;;
  }

  dimension: venue_id {
    type: string
    # hidden: yes
    sql: ${TABLE}."VENUE_ID" ;;
  }

  dimension: venue_name {
    type: string
    sql: ${TABLE}."VENUE_NAME" ;;
  }

  dimension: venue_postal_cd_sgmt_1 {
    type: string
    sql: ${TABLE}."VENUE_POSTAL_CD_SGMT_1" ;;
  }

  dimension: venue_postal_cd_sgmt_2 {
    type: string
    sql: ${TABLE}."VENUE_POSTAL_CD_SGMT_2" ;;
  }

  dimension: venue_state {
    type: string
    sql: ${TABLE}."VENUE_STATE" ;;
  }

  dimension: venue_street_addr {
    type: string
    sql: ${TABLE}."VENUE_STREET_ADDR" ;;
  }

  dimension: venue_street_addr_2 {
    type: string
    sql: ${TABLE}."VENUE_STREET_ADDR_2" ;;
  }

  dimension: venue_sub_type_cd {
    type: string
    sql: ${TABLE}."VENUE_SUB_TYPE_CD" ;;
  }

  dimension: venue_type_cd {
    type: string
    sql: ${TABLE}."VENUE_TYPE_CD" ;;
  }

  dimension: venue_zipcode {
    type: string
    sql: ${TABLE}."VENUE_ZIPCODE" ;;
  }

  measure: count {
    type: count
    drill_fields: [detail*]
  }

  # ----- Sets of fields for drilling ------
  set: detail {
    fields: [
      host_venue_id,
      venue_dma_name,
      venue_name,
      venue.venue_dma_name,
      venue.venue_name,
      venue.host_venue_id,
      venue.count
    ]
  }
}
