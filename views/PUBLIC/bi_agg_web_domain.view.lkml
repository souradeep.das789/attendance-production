view: bi_agg_web_domain {
  sql_table_name: "PUBLIC"."BI_AGG_WEB_DOMAIN"
    ;;

  dimension: adj_type {
    type: string
    sql: ${TABLE}."ADJ_TYPE" ;;
  }

  dimension: adp_visit_cnt {
    type: number
    sql: ${TABLE}."ADP_VISIT_CNT" ;;
  }

  dimension: camefrom_cd {
    type: string
    sql: ${TABLE}."CAMEFROM_CD" ;;
  }

  dimension: cart_visit_cnt {
    type: number
    sql: ${TABLE}."CART_VISIT_CNT" ;;
  }

  dimension: device_type_nm {
    type: string
    sql: ${TABLE}."DEVICE_TYPE_NM" ;;
  }

  dimension: domain_nm {
    type: string
    sql: ${TABLE}."DOMAIN_NM" ;;
  }

  dimension: edp_resale_visit_cnt {
    type: number
    sql: ${TABLE}."EDP_RESALE_VISIT_CNT" ;;
  }

  dimension: edp_visit_cnt {
    type: number
    sql: ${TABLE}."EDP_VISIT_CNT" ;;
  }

  dimension: entry_page_cat_nm {
    type: string
    sql: ${TABLE}."ENTRY_PAGE_CAT_NM" ;;
  }

  dimension: intraday_flg {
    type: string
    sql: ${TABLE}."INTRADAY_FLG" ;;
  }

  dimension_group: max_source_dttm {
    type: time
    timeframes: [
      raw,
      time,
      date,
      week,
      month,
      quarter,
      year
    ]
    sql: ${TABLE}."MAX_SOURCE_DTTM" ;;
  }

  dimension: platinum_ord_qty {
    type: number
    sql: ${TABLE}."PLATINUM_ORD_QTY" ;;
  }

  dimension: platinum_revenue_amt {
    type: number
    sql: ${TABLE}."PLATINUM_REVENUE_AMT" ;;
  }

  dimension: platinum_tkt_qty {
    type: number
    sql: ${TABLE}."PLATINUM_TKT_QTY" ;;
  }

  dimension: reporting_suite_nm {
    type: string
    sql: ${TABLE}."REPORTING_SUITE_NM" ;;
  }

  dimension: reporting_system_nm {
    type: string
    sql: ${TABLE}."REPORTING_SYSTEM_NM" ;;
  }

  dimension: resale_ord_qty {
    type: number
    sql: ${TABLE}."RESALE_ORD_QTY" ;;
  }

  dimension: resale_revenue_amt {
    type: number
    sql: ${TABLE}."RESALE_REVENUE_AMT" ;;
  }

  dimension: resale_tkt_qty {
    type: number
    sql: ${TABLE}."RESALE_TKT_QTY" ;;
  }

  dimension_group: sf_insert_ts {
    type: time
    timeframes: [
      raw,
      time,
      date,
      week,
      month,
      quarter,
      year
    ]
    sql: ${TABLE}."SF_INSERT_TS" ;;
  }

  dimension_group: sf_update_ts {
    type: time
    timeframes: [
      raw,
      time,
      date,
      week,
      month,
      quarter,
      year
    ]
    sql: ${TABLE}."SF_UPDATE_TS" ;;
  }

  dimension: total_ord_qty {
    type: number
    sql: ${TABLE}."TOTAL_ORD_QTY" ;;
  }

  dimension: total_revenue_amt {
    type: number
    sql: ${TABLE}."TOTAL_REVENUE_AMT" ;;
  }

  dimension: total_tkt_qty {
    type: number
    sql: ${TABLE}."TOTAL_TKT_QTY" ;;
  }

  dimension: total_visit_cnt {
    type: number
    sql: ${TABLE}."TOTAL_VISIT_CNT" ;;
  }

  dimension: tracking_cd {
    type: string
    sql: ${TABLE}."TRACKING_CD" ;;
  }

  dimension: utm_campn_cd {
    type: string
    sql: ${TABLE}."UTM_CAMPN_CD" ;;
  }

  dimension: utm_source {
    type: string
    sql: ${TABLE}."UTM_SOURCE" ;;
  }

  dimension_group: visit_start_dt {
    type: time
    timeframes: [
      raw,
      date,
      week,
      month,
      quarter,
      year
    ]
    convert_tz: no
    datatype: date
    sql: ${TABLE}."VISIT_START_DT" ;;
  }

  dimension: waterfall_cat_nm {
    type: string
    sql: ${TABLE}."WATERFALL_CAT_NM" ;;
  }

  measure: count {
    type: count
    drill_fields: []
  }
}
