view: attendance_delivery_channel {
  sql_table_name: "PUBLIC"."ATTENDANCE_DELIVERY_CHANNEL"
    ;;

  dimension: delivery_channel_cat {
    type: string
    sql: ${TABLE}."DELIVERY_CHANNEL_CAT" ;;
  }

  dimension: delivery_channel_cd {
    type: string
    sql: ${TABLE}."DELIVERY_CHANNEL_CD" ;;
  }

  dimension: delivery_channel_desc {
    type: string
    sql: ${TABLE}."DELIVERY_CHANNEL_DESC" ;;
  }

  dimension: delivery_channel_nm {
    type: string
    sql: ${TABLE}."DELIVERY_CHANNEL_NM" ;;
  }

  dimension_group: insert_ts {
    type: time
    timeframes: [
      raw,
      time,
      date,
      week,
      month,
      quarter,
      year
    ]
    sql: ${TABLE}."INSERT_TS" ;;
  }

  dimension_group: update_ts {
    type: time
    timeframes: [
      raw,
      time,
      date,
      week,
      month,
      quarter,
      year
    ]
    sql: ${TABLE}."UPDATE_TS" ;;
  }

  measure: count {
    type: count
    drill_fields: []
  }
}
