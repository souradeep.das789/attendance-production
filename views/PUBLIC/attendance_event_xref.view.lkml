view: attendance_event_xref {
  sql_table_name: "PUBLIC"."ATTENDANCE_EVENT_XREF"
    ;;

  dimension: arch_event_id {
    type: string
    sql: ${TABLE}."ARCH_EVENT_ID" ;;
  }

  dimension: client_cd {
    type: string
    sql: ${TABLE}."CLIENT_CD" ;;
  }

  dimension: event_id_hex {
    type: string
    sql: ${TABLE}."EVENT_ID_HEX" ;;
  }

  dimension_group: insert_ts {
    type: time
    timeframes: [
      raw,
      time,
      date,
      week,
      month,
      quarter,
      year
    ]
    sql: ${TABLE}."INSERT_TS" ;;
  }

  dimension: scan_event_id {
    type: string
    sql: ${TABLE}."SCAN_EVENT_ID" ;;
  }

  dimension_group: update_ts {
    type: time
    timeframes: [
      raw,
      time,
      date,
      week,
      month,
      quarter,
      year
    ]
    sql: ${TABLE}."UPDATE_TS" ;;
  }

  dimension: upsert_type {
    type: string
    sql: ${TABLE}."UPSERT_TYPE" ;;
  }

  measure: count {
    type: count
    drill_fields: []
  }
}
