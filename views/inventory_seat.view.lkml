view: inventory_seat {
  sql_table_name: "CLIENT_ANALYTICS"."INVENTORY_SEAT"
    ;;

  dimension: event_id {
    type: string
    sql: ${TABLE}."EVENT_ID" ;;
  }

  dimension_group: insert_ts {
    type: time
    timeframes: [
      raw,
      time,
      date,
      week,
      month,
      quarter,
      year
    ]
    sql: ${TABLE}."INSERT_TS" ;;
  }

  dimension: inventory_src_nm {
    type: string
    sql: ${TABLE}."INVENTORY_SRC_NM" ;;
  }

  dimension_group: msg_upload_ts {
    type: time
    timeframes: [
      raw,
      time,
      date,
      week,
      month,
      quarter,
      year
    ]
    sql: ${TABLE}."MSG_UPLOAD_TS" ;;
  }

  dimension: place_id {
    type: string
    sql: ${TABLE}."PLACE_ID" ;;
  }

  dimension: place_nm {
    type: string
    sql: ${TABLE}."PLACE_NM" ;;
  }

  dimension: price_zone_nm {
    type: string
    sql: ${TABLE}."PRICE_ZONE_NM" ;;
  }

  dimension: row_nm {
    type: string
    sql: ${TABLE}."ROW_NM" ;;
  }

  dimension: sect_nm {
    type: string
    sql: ${TABLE}."SECT_NM" ;;
  }

  dimension: status_cat_cd {
    type: string
    sql: ${TABLE}."STATUS_CAT_CD" ;;
  }

  dimension: status_nm {
    type: string
    sql: ${TABLE}."STATUS_NM" ;;
  }

  dimension_group: update_ts {
    type: time
    timeframes: [
      raw,
      time,
      date,
      week,
      month,
      quarter,
      year
    ]
    sql: ${TABLE}."UPDATE_TS" ;;
  }

  measure: count {
    type: count
    drill_fields: []
  }
}
