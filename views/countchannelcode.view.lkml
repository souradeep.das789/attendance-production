view: countchannelcode {
  derived_table: {
    sql: SELECT
          CASE
              WHEN (scan.DELIVERY_CHANNEL_CD) = 'A' THEN 'Android'
              WHEN (scan.DELIVERY_CHANNEL_CD) = 'B' THEN 'Android'
              WHEN (scan.DELIVERY_CHANNEL_CD) = 'C' THEN 'iOS'
              WHEN (scan.DELIVERY_CHANNEL_CD) = 'D' THEN 'iOS'
              WHEN (scan.DELIVERY_CHANNEL_CD) = 'E' THEN 'Web'
              WHEN (scan.DELIVERY_CHANNEL_CD) = 'F' THEN 'Web'
              WHEN (scan.DELIVERY_CHANNEL_CD) = 'G' THEN 'iOS'
              WHEN (scan.DELIVERY_CHANNEL_CD) = 'H' THEN '3rd Party'
              WHEN (scan.DELIVERY_CHANNEL_CD) = 'I' THEN '3rd Party'
              WHEN (scan.DELIVERY_CHANNEL_CD) = 'J' THEN 'Web'
              WHEN (scan.DELIVERY_CHANNEL_CD) = 'K' THEN 'Web'
              WHEN (scan.DELIVERY_CHANNEL_CD) = 'L' THEN 'iOS'
              WHEN (scan.DELIVERY_CHANNEL_CD) = 'M' THEN 'Blackberry'
              WHEN (scan.DELIVERY_CHANNEL_CD) = 'N' THEN 'Android'
              WHEN (scan.DELIVERY_CHANNEL_CD) = 'O' THEN 'iOS'
              WHEN (scan.DELIVERY_CHANNEL_CD) = 'P' THEN '3rd Party'
              WHEN (scan.DELIVERY_CHANNEL_CD) = 'PRINT' THEN 'Printed'
              WHEN (scan.DELIVERY_CHANNEL_CD) = 'Q' THEN '3rd Party'
              WHEN (scan.DELIVERY_CHANNEL_CD) = 'R' THEN 'iOS'
              WHEN (scan.DELIVERY_CHANNEL_CD) = 'S' THEN '3rd Party'
              WHEN (scan.DELIVERY_CHANNEL_CD) = 'T' THEN 'SMS'
              WHEN (scan.DELIVERY_CHANNEL_CD) = 'U' THEN 'SMS'
              WHEN (scan.DELIVERY_CHANNEL_CD) = 'V' THEN '3rd Party'
              WHEN (scan.DELIVERY_CHANNEL_CD) = 'W' THEN '3rd Party'
              WHEN (scan.DELIVERY_CHANNEL_CD) = 'X' THEN '3rd Party'
              WHEN (scan.DELIVERY_CHANNEL_CD) = 'Y' THEN 'Android'
              WHEN (scan.DELIVERY_CHANNEL_CD) = 'Z' THEN '3rd Party'
              WHEN (scan.DELIVERY_CHANNEL_CD) = '@' THEN 'Android'
              WHEN (scan.DELIVERY_CHANNEL_CD) = '^' THEN '3rd Party'
                  WHEN (scan.DELIVERY_CHANNEL_CD) = '&' THEN 'Email'
              WHEN (scan.DELIVERY_CHANNEL_CD) = '+' THEN '3rd Party'
              ELSE 'Unknown'
          END AS channelcode,
          COUNT(lastScan.lastValidScanDate) AS count
      FROM
          BI.PUBLIC.ATTENDANCE_SCAN scan
              INNER JOIN
          (SELECT
              se.scan_event_id,
          se.barcode,
          MAX(se.SCAN_DTTM_UTC) AS lastValidScanDate
          FROM
              BI.PUBLIC.ATTENDANCE_SCAN_EVENT_VW se
              where

          {% condition event_name %} se.event_name  {% endcondition %}
    AND se.SCAN_ENTRY_CAT IN ('ENTRY' , 'REENTRY', 'EXIT', 'INTERNAL', 'INTERNAL_ENTRY', 'INTERNAL_ENTRY_EXIT')
    and {% condition event_date %} se.event_dt  {% endcondition %}
    AND se.ATTENDANCE_SRC_SYS_CD = 'PRESENCE'

    and {% condition venuename %} se.Venue_Name  {% endcondition %}


    and {% condition eventtypedesc %} se.Event_Type_Desc  {% endcondition %}


    and {% condition majorcatname %} se.Major_Cat_Name  {% endcondition %}

    and {% condition minorcatname %} se.Minor_Cat_Name  {% endcondition %}

          AND se.ATTENDANCE_SRC_SYS_CD = 'PRESENCE'
          GROUP BY se.scan_event_id , se.barcode
          ) AS lastScan
          ON scan.scan_event_id = lastScan.scan_event_id
              AND scan.barcode = lastScan.barcode
              AND scan.SCAN_DTTM_UTC = lastScan.lastValidScanDate
      GROUP BY channelcode
       ;;
  }
  filter: event_name {
    type: string
  }

  filter: event_date {
    type: date

  }
  filter: venuename {
    type: string
  }

  filter: safetix {
    type: string
    default_value: "No"
  }

  filter: eventtypedesc {
    type: string

  }
  filter: majorcatname {
    type: string

  }
  filter: minorcatname {
    type: string

  }
  dimension: channelcode {
    type: string
    sql: ${TABLE}."CHANNELCODE" ;;
  }

  dimension: count {
    type: number
    sql: ${TABLE}."COUNT" ;;
  }
  measure: Total {
    type: number
    sql:${TABLE}."COUNT"  ;;
  }

  set: detail {
    fields: [channelcode, count]
  }
}
