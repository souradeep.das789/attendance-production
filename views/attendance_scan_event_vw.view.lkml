view: attendance_scan_event_vw {
  sql_table_name: "PUBLIC"."ATTENDANCE_SCAN_EVENT_VW"
    ;;

  dimension: ROTATING_ENTRY_TOKEN_DTTM {
    type: date_time
    sql: ${TABLE}."ROTATING_ENTRY_TOKEN_DTTM" ;;
  }
  dimension: access_mgr_sys_id {
    type: string
    sql: ${TABLE}."ACCESS_MGR_SYS_ID" ;;
  }

  dimension: arch_event_code {
    type: string
    sql: ${TABLE}."ARCH_EVENT_CODE" ;;
  }

  dimension: arch_event_id {
    type: string
    sql: ${TABLE}."ARCH_EVENT_ID" ;;
  }

  dimension: archtics_result_cd {
    type: number
    sql: ${TABLE}."ARCHTICS_RESULT_CD" ;;
  }

  dimension: attendance_src_sys_cd {
    type: string
    sql: ${TABLE}."ATTENDANCE_SRC_SYS_CD" ;;
  }

  dimension: barcode {
    type: string
    sql: ${TABLE}."BARCODE" ;;
  }

  dimension: base_tkt_type_id {
    type: string
    sql: ${TABLE}."BASE_TKT_TYPE_ID" ;;
  }

  dimension: cancelled_flg {
    type: string
    sql: ${TABLE}."CANCELLED_FLG" ;;
  }

  dimension: client_cd {
    type: string
    sql: ${TABLE}."CLIENT_CD" ;;
  }

  dimension: client_nm {
    type: string
    sql: ${TABLE}."CLIENT_NM" ;;
  }

  dimension: clientbp {
    type: number
    sql: ${TABLE}."CLIENTBP" ;;
  }

  dimension_group: create_dt {
    type: time
    timeframes: [
      raw,
      date,
      week,
      month,
      quarter,
      year
    ]
    convert_tz: no
    datatype: date
    sql: ${TABLE}."CREATE_DT" ;;
  }

  dimension: cur_ind {
    type: string
    sql: ${TABLE}."CUR_IND" ;;
  }

  dimension: cust_id {
    type: string
    sql: ${TABLE}."CUST_ID" ;;
  }

  dimension: delivery_channel_cat {
    type: string
    sql: ${TABLE}."DELIVERY_CHANNEL_CAT" ;;
  }

  dimension: delivery_channel_cd {
    type: string
    sql: ${TABLE}."DELIVERY_CHANNEL_CD" ;;
  }

  dimension: delivery_channel_desc {
    type: string
    sql: ${TABLE}."DELIVERY_CHANNEL_DESC" ;;
  }

  dimension: delivery_channel_nm {
    type: string
    sql: ${TABLE}."DELIVERY_CHANNEL_NM" ;;
  }

  dimension: digital_event_flg {
    type: string
    sql: ${TABLE}."DIGITAL_EVENT_FLG" ;;
  }

  dimension: disc_ven_id {
    type: string
    sql: ${TABLE}."DISC_VEN_ID" ;;
  }

  dimension_group: event_date_time_utc {
    type: time
    timeframes: [
      raw,
      time,
      date,
      week,
      month,
      quarter,
      year
    ]
    sql: ${TABLE}."EVENT_DATE_TIME_UTC" ;;
  }

  dimension: event_disp_name {
    type: string
    sql: ${TABLE}."EVENT_DISP_NAME" ;;
  }

  dimension_group: event_dt {
    type: time
    timeframes: [
      raw,
      date,
      week,
      month,
      quarter,
      year
    ]
    convert_tz: no
    datatype: date
    sql: ${TABLE}."EVENT_DT" ;;
  }

  dimension: event_id_hex {
    type: string
    sql: ${TABLE}."EVENT_ID_HEX" ;;
  }

  dimension: event_id_src_sys_cd {
    type: string
    sql: ${TABLE}."EVENT_ID_SRC_SYS_CD" ;;
  }

  dimension: event_name {
    type: string
    sql: ${TABLE}."EVENT_NAME" ;;
  }

  dimension: event_time {
    type: number
    sql: ${TABLE}."EVENT_TIME" ;;

  }

  dimension: event_type_cd {
    type: string
    sql: ${TABLE}."EVENT_TYPE_CD" ;;
  }

  dimension: event_type_desc {
    type: string
    sql: ${TABLE}."EVENT_TYPE_DESC" ;;
  }

  dimension: exch_active_flg {
    type: string
    sql: ${TABLE}."EXCH_ACTIVE_FLG" ;;
  }

  dimension: fan_device_os_nm {
    type: string
    sql: ${TABLE}."FAN_DEVICE_OS_NM" ;;
  }

  dimension: gate_id {
    type: string
    sql: ${TABLE}."GATE_ID" ;;
  }

  dimension: gate_nm {
    type: string
    sql: ${TABLE}."GATE_NM" ;;
  }

  dimension: ghost_ind {
    type: string
    sql: ${TABLE}."GHOST_IND" ;;
  }

  dimension: host_event_code {
    type: string
    sql: ${TABLE}."HOST_EVENT_CODE" ;;
  }

  dimension: host_event_id {
    type: string
    sql: ${TABLE}."HOST_EVENT_ID" ;;
  }

  dimension_group: insert_ts {
    type: time
    timeframes: [
      raw,
      time,
      date,
      week,
      month,
      quarter,
      year
    ]
    sql: ${TABLE}."INSERT_TS" ;;
  }

  dimension: ism_enabled_flag {
    type: string
    sql: ${TABLE}."ISM_ENABLED_FLAG" ;;
  }

  dimension: major_cat_name {
    type: string
    sql: ${TABLE}."MAJOR_CAT_NAME" ;;
  }

  dimension: minor_cat_name {
    type: string
    sql: ${TABLE}."MINOR_CAT_NAME" ;;
  }

  dimension_group: msg_upload_dttm {
    type: time
    timeframes: [
      raw,
      time,
      date,
      week,
      month,
      quarter,
      year
    ]
    sql: ${TABLE}."MSG_UPLOAD_DTTM" ;;
  }

  dimension: nrep_music_service_flg {
    type: string
    sql: ${TABLE}."NREP_MUSIC_SERVICE_FLG" ;;
  }

  dimension: online_ind {
    type: string
    sql: ${TABLE}."ONLINE_IND" ;;
  }

  dimension_group: onsale_dt {
    type: time
    timeframes: [
      raw,
      date,
      week,
      month,
      quarter,
      year
    ]
    convert_tz: no
    datatype: date
    sql: ${TABLE}."ONSALE_DT" ;;
  }

  dimension: order_system_src_sys_cd {
    type: string
    sql: ${TABLE}."ORDER_SYSTEM_SRC_SYS_CD" ;;
  }

  dimension_group: presale_dt {
    type: time
    timeframes: [
      raw,
      date,
      week,
      month,
      quarter,
      year
    ]
    convert_tz: no
    datatype: date
    sql: ${TABLE}."PRESALE_DT" ;;
  }

  dimension: primary_act_id {
    type: number
    sql: ${TABLE}."PRIMARY_ACT_ID" ;;
  }

  dimension: primary_act_name {
    type: string
    sql: ${TABLE}."PRIMARY_ACT_NAME" ;;
  }

  dimension: primary_ingress_ind {
    type: string
    sql: ${TABLE}."PRIMARY_INGRESS_IND" ;;
  }

  dimension: print_cnt {
    type: number
    sql: ${TABLE}."PRINT_CNT" ;;
  }

  dimension: promoter_name {
    type: string
    sql: ${TABLE}."PROMOTER_NAME" ;;
  }

  dimension: reject_cd {
    type: string
    sql: ${TABLE}."REJECT_CD" ;;
  }

  dimension: reject_reason_nm {
    type: string
    sql: ${TABLE}."REJECT_REASON_NM" ;;
  }

  dimension: row_num {
    type: string
    sql: ${TABLE}."ROW_NUM" ;;
  }

  dimension: row_sys_id {
    type: number
    sql: ${TABLE}."ROW_SYS_ID" ;;
  }

  dimension_group: scan_dt_utc {
    type: time
    timeframes: [
      raw,
      date,
      week,
      month,
      quarter,
      year
    ]
    convert_tz: no
    datatype: date
    sql: ${TABLE}."SCAN_DT_UTC" ;;
  }

  dimension_group: scan_dttm_utc {
    type: time
    timeframes: [
      raw,
      time,
      date,
      week,
      month,
      quarter,
      year
    ]
    sql: ${TABLE}."SCAN_DTTM_UTC" ;;
  }

  dimension: scan_entry_cat {
    type: string
    sql: ${TABLE}."SCAN_ENTRY_CAT" ;;
  }

  dimension: scan_event_cd {
    type: string
    sql: ${TABLE}."SCAN_EVENT_CD" ;;
  }

  dimension: scan_event_id {
    type: string
    sql: ${TABLE}."SCAN_EVENT_ID" ;;
  }

  dimension: scan_result_cat {
    type: string
    sql: ${TABLE}."SCAN_RESULT_CAT" ;;
  }

  dimension: scan_result_cd {
    type: string
    sql: ${TABLE}."SCAN_RESULT_CD" ;;
  }

  dimension: scan_result_desc {
    type: string
    sql: ${TABLE}."SCAN_RESULT_DESC" ;;
  }

  dimension: scan_result_nm {
    type: string
    sql: ${TABLE}."SCAN_RESULT_NM" ;;
  }

  dimension: scan_seq_num {
    type: number
    sql: ${TABLE}."SCAN_SEQ_NUM" ;;
  }

  dimension: scan_token_id {
    type: string
    sql: ${TABLE}."SCAN_TOKEN_ID" ;;
  }

  dimension: scanner_device_id {
    type: string
    sql: ${TABLE}."SCANNER_DEVICE_ID" ;;
  }

  dimension: scanner_device_nm {
    type: string
    sql: ${TABLE}."SCANNER_DEVICE_NM" ;;
  }

  dimension: scanner_software_version_num {
    type: string
    sql: ${TABLE}."SCANNER_SOFTWARE_VERSION_NUM" ;;
  }

  dimension: seat_cd {
    type: string
    sql: ${TABLE}."SEAT_CD" ;;
  }

  dimension: seat_num {
    type: string
    sql: ${TABLE}."SEAT_NUM" ;;
  }

  dimension: secondary_act_id {
    type: number
    sql: ${TABLE}."SECONDARY_ACT_ID" ;;
  }

  dimension: secondary_act_name {
    type: string
    sql: ${TABLE}."SECONDARY_ACT_NAME" ;;
  }

  dimension: sect_nm {
    type: string
    sql: ${TABLE}."SECT_NM" ;;
  }

  dimension: settle_cd {
    type: string
    sql: ${TABLE}."SETTLE_CD" ;;
  }

  dimension: ticket_id {
    type: string
    sql: ${TABLE}."TICKET_ID" ;;
  }

  dimension: ticket_text {
    type: string
    sql: ${TABLE}."TICKET_TEXT" ;;
  }

  dimension: tkt_status_cd {
    type: string
    sql: ${TABLE}."TKT_STATUS_CD" ;;
  }

  dimension: tkt_status_nm {
    type: string
    sql: ${TABLE}."TKT_STATUS_NM" ;;
  }

  dimension_group: update_ts {
    type: time
    timeframes: [
      raw,
      time,
      date,
      week,
      month,
      quarter,
      year
    ]
    sql: ${TABLE}."UPDATE_TS" ;;
  }

  dimension: valid_ticketed_event_flag {
    type: string
    sql: ${TABLE}."VALID_TICKETED_EVENT_FLAG" ;;
  }

  dimension: ven_id {
    type: number
    sql: ${TABLE}."VEN_ID" ;;
  }

  dimension: ven_id_src_sys_cd {
    type: string
    sql: ${TABLE}."VEN_ID_SRC_SYS_CD" ;;
  }

  dimension: venue_city {
    type: string
    sql: ${TABLE}."VENUE_CITY" ;;
  }

  dimension: venue_name {
    type: string
    sql: ${TABLE}."VENUE_NAME" ;;
  }

  dimension: venue_state {
    type: string
    sql: ${TABLE}."VENUE_STATE" ;;
  }

  dimension: venue_zipcode {
    type: string
    sql: ${TABLE}."VENUE_ZIPCODE" ;;
  }

  dimension: zone_cd {
    type: number
    sql: ${TABLE}."ZONE_CD" ;;
  }


  dimension: em_application {
    sql: CASE
                   WHEN ${delivery_channel_cd} = 'A' THEN 'Ticketmaster'
                   WHEN ${delivery_channel_cd} = 'B' THEN 'Live Nation'
                   WHEN ${delivery_channel_cd} = 'C' THEN 'Ticketmaster'
                   WHEN ${delivery_channel_cd} = 'D' THEN 'Live Nation'
                   WHEN ${delivery_channel_cd} = 'E' THEN 'Ticketmaster'
                   WHEN ${delivery_channel_cd} = 'F' THEN 'Live Nation'
                   WHEN ${delivery_channel_cd} = 'G' THEN 'Wallet'
                   WHEN ${delivery_channel_cd} = 'H' THEN 'Goldstar'
                   WHEN ${delivery_channel_cd} = 'I' THEN 'Gametime'
                   WHEN ${delivery_channel_cd} = 'J' THEN 'Account Manager Web'
                   WHEN ${delivery_channel_cd} = 'K' THEN 'Account Manager Web'
                   WHEN ${delivery_channel_cd} = 'L' THEN 'Account Manager'
                   WHEN ${delivery_channel_cd} = 'M' THEN 'Account Manager'
                   WHEN ${delivery_channel_cd} = 'N' THEN 'Account Manager'
                   WHEN ${delivery_channel_cd} = 'O' THEN 'Account Manager'
                   WHEN ${delivery_channel_cd} = 'P' THEN 'Groupon'
                   WHEN ${delivery_channel_cd} = 'PRINT' THEN 'Printed'
                   WHEN ${delivery_channel_cd} = 'Q' THEN 'StubHub'
                   WHEN ${delivery_channel_cd} = 'R' THEN 'Wallet'
                   WHEN ${delivery_channel_cd} = 'S' THEN 'SeatGeek'
                   WHEN ${delivery_channel_cd} = 'T' THEN 'TM1 Sales'
                   WHEN ${delivery_channel_cd} = 'U' THEN 'Archtics SMS'
                   WHEN ${delivery_channel_cd} = 'V' THEN 'Venue'
                   WHEN ${delivery_channel_cd} = 'W' THEN 'Wristband'
                   WHEN ${delivery_channel_cd} = 'X' THEN 'MLB'
                   WHEN ${delivery_channel_cd} = 'Y' THEN 'Wallet'
                   WHEN ${delivery_channel_cd} = 'Z' THEN 'GameDay'
                   WHEN ${delivery_channel_cd} = '@' THEN 'Wallet'
                   WHEN ${delivery_channel_cd} = '^' THEN 'VividSeats'
                   WHEN ${delivery_channel_cd} = '+' THEN 'Booking.com'
                   ELSE 'Unknown'
            END ;;
  }
  dimension: em_platform {
    sql: CASE
                   WHEN ${delivery_channel_cd} = 'A' THEN 'Android'
                   WHEN ${delivery_channel_cd} = 'B' THEN 'Android'
                   WHEN ${delivery_channel_cd} = 'C' THEN 'iOS'
                   WHEN ${delivery_channel_cd} = 'D' THEN 'iOS'
                   WHEN ${delivery_channel_cd} = 'E' THEN 'Web'
                   WHEN ${delivery_channel_cd} = 'F' THEN 'Web'
                   WHEN ${delivery_channel_cd} = 'G' THEN 'iOS'
                   WHEN ${delivery_channel_cd} = 'H' THEN '3rd Party'
                   WHEN ${delivery_channel_cd} = 'I' THEN '3rd Party'
                   WHEN ${delivery_channel_cd} = 'J' THEN 'Android'
                   WHEN ${delivery_channel_cd} = 'K' THEN 'iOS'
                   WHEN ${delivery_channel_cd} = 'L' THEN 'iOS'
                   WHEN ${delivery_channel_cd} = 'M' THEN 'Blackberry'
                   WHEN ${delivery_channel_cd} = 'N' THEN 'Android'
                   WHEN ${delivery_channel_cd} = 'O' THEN 'iOS'
                   WHEN ${delivery_channel_cd} = 'P' THEN '3rd Party'
                   WHEN ${delivery_channel_cd} = 'PRINT' THEN 'Printed'
                   WHEN ${delivery_channel_cd} = 'Q' THEN '3rd Party'
                   WHEN ${delivery_channel_cd} = 'R' THEN 'iOS'
                   WHEN ${delivery_channel_cd} = 'S' THEN '3rd Party'
                   WHEN ${delivery_channel_cd} = 'T' THEN 'SMS'
                   WHEN ${delivery_channel_cd} = 'U' THEN 'SMS'
                   WHEN ${delivery_channel_cd} = 'V' THEN '3rd Party'
                   WHEN ${delivery_channel_cd} = 'W' THEN '3rd Party'
                   WHEN ${delivery_channel_cd} = 'X' THEN '3rd Party'
                   WHEN ${delivery_channel_cd} = 'Y' THEN 'Android'
                   WHEN ${delivery_channel_cd} = 'Z' THEN 'Gameday'
                   WHEN ${delivery_channel_cd} = '@' THEN 'Android'
                   WHEN ${delivery_channel_cd} = '^' THEN '3rd Party'
                   WHEN ${delivery_channel_cd} = '+' THEN '3rd Party'
                   ELSE 'Unknown'
            END ;;
  }
  dimension: em_technology {
    sql: CASE
                   WHEN ${fan_device_os_nm} = 'rotating-ios' THEN 'Rotating'
                   WHEN ${fan_device_os_nm} = 'IOS' THEN 'NFC'
                   WHEN ${fan_device_os_nm} = 'rotating-android' THEN 'Rotating'
                   WHEN ${fan_device_os_nm} = 'ANDROID' THEN 'NFC'
                   WHEN ${fan_device_os_nm} = 'rotating-other' THEN 'Rotating'
                   WHEN ${fan_device_os_nm} = 'smart' THEN 'Smart Tickets'
                   WHEN ${fan_device_os_nm} = 'printed' THEN 'Printed'
                   ELSE 'Unknown'
            END ;;
  }

  dimension_group: event_date_time {
    type: time
    timeframes: [
      raw,
      time,
      date,
      week,
      month,
      quarter,
      year
    ]
    sql:to_timestamp_ltz( ${event_dttm_st}) ;;
  }

  dimension: event_time_6{
    type:  string
    sql: lpad(${event_time},6,'0') ;;
  }

  dimension: event_dttm_st{
    type:  string
    sql: concat(${event_dt_date},' ',substr(${event_time_6},1,2),':',substr(${event_time_6},3,2)) ;;
  }

  dimension: scan_offset_min{
    type: number
    sql: datediff(minute, ${event_date_time_raw}, to_timestamp_tz(${scan_rate_minute15})) ;;
  }

  dimension: scan_offset_min_no_tf{
    type: number
    sql: case
          WHEN ${event_dt_year} = '2018' and ${primary_act_name} = 'Minnesota Vikings' THEN datediff(minute, ${event_date_time_raw}, ${scan_dttm_utc_raw})
          WHEN ${event_dt_year} = '2019' and ${primary_act_name} = 'Minnesota Vikings' THEN datediff(minute, ${event_date_time_raw}, ${scan_dttm_utc_raw})-300
          ELSE datediff(minute, ${event_date_time_raw}, ${scan_dttm_utc_raw})-240
          END;;
  }

  dimension: scan_offset_min_no_tf_2{
    type: number
    sql: case
          WHEN ${event_dt_year} = to_date('2018') THEN datediff(minute, ${event_date_time_raw}, ${scan_dttm_utc_raw})
          ELSE datediff(minute, ${event_date_time_raw}, ${scan_dttm_utc_raw})-240
          END;;
  }


#
# general
#
  dimension: ticketId_sequenceNum {
    type: string
    sql: ${ticket_id} || '-' || ${scan_seq_num} ;;
  }

#
# Scan Rate
#
  dimension: Entry {
    type: number
    sql: ${scan_entry_cat} = 'ENTRY' ;;
  }
  dimension_group: scan_rate_1 {
    type:  time
    timeframes: [minute, minute10, minute15, minute30]
    sql: ${scan_dttm_utc_date} ;;
    convert_tz: no
  }
  dimension_group: scan_rate {
    type:  time
    timeframes: [minute, minute10, minute15, minute30]
    sql: ${scan_dttm_utc_raw} ;;
    convert_tz: no
  }
  dimension_group: scan_rate_tz {
    type:  time
    timeframes: [minute, minute10, minute15, minute30]
    sql: ${scan_dttm_utc_raw} ;;
    convert_tz: yes
  }
#
# distinct member id
#

  measure: number_of_unique_memberid {
    type: count_distinct
    sql: ${cust_id} ;;
  }

#
# distinct member it
#

  measure: missing_memberid {
    type: count
    filters: {
      field: cust_id
      value: "EMPTY"

    }
    filters: {
      field: is_total_attendance_scan
      value: "yes"
    }
    filters: {
      field: em_technology
      value: "NFC, Rotating"
    }
  }

#
# total attendance
#
  dimension: is_total_attendance_scan {
    type: yesno
    sql: ${scan_entry_cat} IN ('EXIT', 'ENTRY','INTERNAL_ENTRY') ;;
  }

  dimension: total_attendance_ticketId_seqNum {
    type: string
    sql: CASE WHEN ${is_total_attendance_scan} THEN ${ticketId_sequenceNum} END ;;
  }

  measure: total_attendance {
    label: "Total Attendance"
    type: count_distinct
    sql: ${total_attendance_ticketId_seqNum} ;;
  }
##################
  dimension: is_total_attendance_scan1 {
    type: yesno
    sql: ${scan_entry_cat} IN ('INTERNAL_EXIT_REJECT','INTERNAL_REJECT','REJECT','EXIT_REJECT') ;;
  }

  dimension: total_attendance_ticketId_seqNum1 {
    type: string
    sql: CASE WHEN ${is_total_attendance_scan1} THEN ${ticketId_sequenceNum} END ;;
  }

  measure: total_attendance1 {
    label: "Total Attendance1"
    type: count_distinct
    sql: ${total_attendance_ticketId_seqNum1} ;;
  }

#
#
# total exits
#

  dimension: is_exit_scan {
    type: yesno
    sql: ${scan_entry_cat} = 'EXIT' ;;
  }

  measure: total_distinct_exits{
    label: "Total Exits"
    type: count_distinct
    sql: ${ticketId_sequenceNum} ;;
    filters: {
      field: is_exit_scan
      value: "yes"
    }
  }

  measure: total_exits {
    label: "Total Scans Out"
    type: count
    filters: {
      field: is_exit_scan
      value: "yes"
    }
  }

#
# Total Scans In
#
  dimension: is_entry_scan {
    type: yesno
    sql: ${scan_entry_cat} IN ('ENTRY', 'REENTRY') ;;
  }

  dimension: TempScansIn {
    type: string
    sql:${scan_entry_cat} IN ('ENTRY', 'REENTRY') then ${barcode}  ;;
  }

  measure: total_scans_in {
    label: "Total Scans In"
    type: count
    filters: {
      field: is_entry_scan
      value: "yes"

    }
  }

  dimension: REJ_Cal {
    type: yesno
    sql: ${scan_entry_cat} IN('ENTRY','REENTRY','EXIT','INTERNAL','INTERNAL_ENTRY','INTERNAL_ENTRY_EXIT') ;;
  }
  measure: REJ {
    label: "REJ"
    type: count
    filters: {
      field: REJ_Cal
      value: "No"
    }
  }

  dimension: REJM7_Cal {
    type: yesno
    sql: ${attendance_src_sys_cd} = 'PRESENCE' AND ${scan_result_cd} = 'NOT_FOUND' AND ${delivery_channel_cd} <> 'PRINT' ;;
  }

  measure: REJM7 {
    label: "REJM7"
    type: count
    filters: {
      field: REJM7_Cal
      value: "Yes"
    }
  }
  dimension: M7FXD_Cal {
    type: yesno
    sql: ${REJM7_Cal} = 'yes' AND ${scan_entry_cat} = 'ENTRY' ;;
  }

  measure: M7FXD {
    label: "M7FXD"
    type: count
    filters: {
      field: M7FXD_Cal
      value: "Yes"
    }
  }


  dimension: REJP7_Cal {
    type: yesno
    sql: ${attendance_src_sys_cd} = 'PRESENCE' AND ${scan_result_cd} = 'NOT_FOUND' AND ${delivery_channel_cd} = 'PRINT' ;;
  }

  measure: REJP7 {
    label: "REJP7"
    type: count
    filters: {
      field: REJP7_Cal
      value: "Yes"
    }
  }
  dimension: P7FXD_Cal {
    type: yesno
    sql: ${REJP7_Cal} = 'yes' AND ${scan_entry_cat} = 'ENTRY' ;;
  }

  measure: P7FXD {
    label: "P7FXD"
    type: count
    filters: {
      field: P7FXD_Cal
      value: "Yes"
    }
  }

#
# current in
#
  dimension: exit_ticketid_seqNum {
    type: string
    sql: CASE WHEN ${is_exit_scan} THEN ${ticketId_sequenceNum} END ;;
  }

  dimension: is_reentry_scan{
    type: yesno
    sql: ${scan_entry_cat} = 'REENTRY' ;;
  }

  measure: total_reentries {
    type: count
    filters: {
      field: is_reentry_scan
      value: "yes"
    }
  }

  measure: current_in {
    label: "Current In"
    type: number
    sql: ${total_attendance} - ${total_exits} + ${total_reentries} + ${total_distinct_exits} ;;
  }

#
# rejects
#
  dimension: is_reject_scan {
    type: yesno
    sql:  ${scan_entry_cat} IN ('INTERNAL_EXIT_REJECT','INTERNAL_REJECT', 'REJECT','EXIT_REJECT') OR ${scan_result_cd} = ' ' ;;

  }

  # dimension: is_reject_scan {
  #  type: yesno
  #  sql: ${scan_result_cd} = 'REJECT' OR ${scan_entry_cat} IN ('INTERNAL_EXIT_REJECT','INTERNAL_REJECT', 'REJECT') ;;
  #}

  measure: total_rejects {
    label: "Rejects"
    type: count
    filters: {
      field: is_reject_scan
      value: "yes"
    }
    drill_fields: [scan_entry_cat,total_rejects]

  }

#
# internal
#
  dimension: Safetix {
    type: string
    sql: case when ${fan_device_os_nm}  LIKE '%rotating%'  then 'Yes'
        else
        case when ${fan_device_os_nm} IN ('android' , 'ANDROID', 'IOS', 'ios') then 'Yes'
        else 'No'

        end;;
        #sql: ${fan_device_os_nm} LIKE '%rotating%' AND ${fan_device_os_nm} IN ('android' , 'ANDROID', 'IOS', 'ios')  ;;
    }
    dimension: is_internal_scan {
      type: yesno
      sql: ${scan_result_cd} IN ('INTERNAL_SCAN', 'INTERNAL_SCAN_OFFLINE','INTERNAL_SCAN_VALIDATOR');;
    }

    measure: total_internals {
      label: "Internal"
      type: count
      filters: {
        field: is_internal_scan
        value: "yes"
      }
    }


    measure: count_distinct_events {
      type: count_distinct
      sql: ${event_id_hex} ;;
      label: "Events"
    }
#
# current out
#
    dimension: Mobile1 {
      type: string
      sql: CASE WHEN ${delivery_channel_cd} is not null and  ${delivery_channel_cd} != ' ' and  ${delivery_channel_cd} != 'PRINT' THEN 'Yes'
          else 'No'
          END;;
    }

    measure: CountMobile {
      label: "Mobile%"
      type: count
      filters: {
        field: Mobile1
        value: "Yes"
      }
    }

    dimension: Mobile2 {
      type: string
      sql: CASE WHEN  ${delivery_channel_cat} = 'Mobile' THEN 'Yes'
          else 'No'
          END;;
    }

    measure: CountMobile2 {
      label: "Mobile2%"
      type: count
      filters: {
        field: Mobile2
        value: "Yes"
      }
    }
    measure: CountMobile3 {
      label: "CountMobile3"
      type: count_distinct
      sql: ${total_attendance_ticketId_seqNum}  ;;
      filters: [delivery_channel_cat: "Mobile"]
    }

    measure: count {
      type: count
      drill_fields: [detail*]
      view_label: "Scan Counts"
    }

    dimension: Wallet {
      type: string
      sql: CASE WHEN ${fan_device_os_nm} IN ('IOS', 'ios') THEN 'Yes'
            else 'No'
            END;;
    }

    dimension: GooglePayWallet {
      type: string
      sql: CASE WHEN ${fan_device_os_nm} IN ('rotating-android', 'ROTATING-ANDROID') and ${delivery_channel_cd} IN ('@','Y') Then  'Yes'
          else 'No'
          END;;
    }
    dimension: Paper {
      type: string
      sql: CASE WHEN ${delivery_channel_cd} = 'PRINT'   THEN 'Yes'
          else 'No'
          END;;
    }
    measure: PaperCount_1 {
      type: count
      filters: {
        field: Paper
        value: "Yes"
      }
    }

    measure: PaperCount {
      label: "PaperCount"
      type: count_distinct
      sql: ${total_attendance_ticketId_seqNum}  ;;
      filters: [Paper: "Yes"]
    }


    dimension: Cards {
      type: string
      sql: CASE WHEN  (${delivery_channel_cd} =  'PRINT' or NULL) and (${arch_event_code} LIKE '%[_]%')   THEN 'Yes'
          else 'No'
          END;;
    }

    dimension: QR {
      type: string
      sql: CASE WHEN ${delivery_channel_cd} !=  'PRINT' and  ${fan_device_os_nm} = 'printed'  THEN 'Yes'
          else 'No'
          END;;
    }

    dimension: OfflineSacn {
      type: string
      sql: CASE WHEN  ${scan_result_cd} like  '%OFFLINE'  THEN 'Yes'
          else 'No'
          END;;
    }

    measure: OfflineSacnCount {
      type: count
      filters: {
        field: OfflineSacn
        value: "Yes"
      }
    }

    dimension: ScanOffline {
      type: string
      sql: CASE WHEN  ${OfflineSacn} = 'Yes' THEN 'Yes'
          else 'No'
          END;;
    }

    measure: ScanOffline1 {
      type: count
      filters: {
        field: ScanOffline
        value: "Yes"
      }
    }


    dimension: RetCalculation1 {
      type: string
      sql: CASE WHEN ${fan_device_os_nm} IN ('rotating-android','rotating-ios','rotating-other') and ${fan_device_os_nm} is not null THEN 'Yes'
          else 'No'
          END;;
    }
    dimension: RetCalculation2 {
      type: string
      sql: CASE WHEN ${delivery_channel_cd} = '@' THEN 'No'
          else 'Yes'
          END;;
    }
    measure: RetCalculation3 {
      type: count
      filters: {
        field: RetCalculation1
        value: "Yes"
      }
    }

    measure: RetCalculation4 {
      type: count
      filters: {
        field: RetCalculation2
        value: "Yes"
      }
    }


    measure: Ret {
      label: "Ret"
      type: count
      filters: {
        field: RetCalculation1
        value: "Yes"
      }
    }

    dimension: RetHigh {
      type: string
      sql: CASE WHEN ${fan_device_os_nm} LIKE 'rotating%' OR ${fan_device_os_nm} LIKE 'ROTATING%' and ${scan_entry_cat} IN ('EXIT', 'ENTRY','INTERNAL_ENTRY')
          THEN 'Yes'
          else 'No'
          END;;
    }


    measure: RetHigh1 {
      label: "RetHigh1"
      type: count_distinct
      sql: ${total_attendance_ticketId_seqNum}  ;;
      filters: [RetHigh: "Yes"]
    }
    dimension: HighPaper {
      type: string
      sql: CASE WHEN ${delivery_channel_cd} is null or ' ' and ${fan_device_os_nm} = 'printed' THEN 'Yes'
          else 'No'
          END;;
    }

    measure: HighPaper1 {
      label: "HighPaper1"
      type: count_distinct
      sql: ${total_attendance_ticketId_seqNum}  ;;
      filters: [HighPaper: "Yes"]
    }

    dimension: Gpay {
      type: string
      sql: CASE WHEN ${delivery_channel_cd} in ('@','Y') and ${fan_device_os_nm} IN ('rotating-android','ROTATING-ANDROID') THEN 'Yes'
          else 'No'
          END;;
    }


    # measure: Gpay1 {
    #   label: "Gpay1"
    #   type: count
    #   filters: {
    #     field: Gpay
    #     value: "Yes"
    #   }
    # }

    measure: Gpay1 {
      label: "Gpay1"
      type: count_distinct
      sql: ${total_attendance_ticketId_seqNum}  ;;
      filters: [Gpay: "Yes"]
    }


    dimension: QRHigh {
      type: string
      sql: CASE WHEN ${delivery_channel_cd} !='PRINT' and ${fan_device_os_nm} = 'printed' THEN 'Yes'
          else 'No'
          END;;
    }


    # measure: HighQR1 {
    #   label: "HighQR1"
    #   type: count
    #   filters: {
    #     field: QRHigh
    #     value: "Yes"
    #   }
    # }

    measure: HighQR1 {
      label: "HighQR1"
      type: count_distinct
      sql: ${total_attendance_ticketId_seqNum}  ;;
      filters: [QRHigh: "Yes"]
    }


    dimension: SDK {
      type: string
      sql: CASE WHEN ${delivery_channel_cd} in ('N','O','R','@') THEN 'Yes'
          else 'No'
          END;;
    }



    # measure: SDK1 {
    #   label: "SDK1"
    #   type: count
    #   filters: {
    #     field: SDK
    #     value: "Yes"
    #   }
    # }

    measure: SDK1 {
      label: "SDK1"
      type: count_distinct
      sql: ${total_attendance_ticketId_seqNum}  ;;
      filters: [SDK: "Yes"]
    }


    dimension: Tmapp {
      type: string
      sql: CASE WHEN ${delivery_channel_cd} in ('A','C') THEN 'Yes'
          else 'No'
          END;;
    }

    # measure: Tmapp1 {
    #   label: "Tmapp1"
    #   type: count
    #   filters: {
    #     field: Tmapp
    #     value: "Yes"
    #   }
    # }
    measure: Tmapp1 {
      label: "Tmapp1"
      type: count_distinct
      sql: ${total_attendance_ticketId_seqNum}  ;;
      filters: [Tmapp: "Yes"]
    }
    dimension: AcctMgr {
      type: string
      sql: CASE WHEN ${delivery_channel_cd} in ('J','K','L','M','Y') THEN 'Yes'
          else 'No'
          END;;
    }

    # measure: AcctMgr1 {
    #   label: "AcctMgr1"
    #   type: count
    #   filters: {
    #     field: AcctMgr
    #     value: "Yes"
    #   }
    # }

    measure: AcctMgr1 {
      label: "AcctMgr1"
      type: count_distinct
      sql: ${total_attendance_ticketId_seqNum}  ;;
      filters: [AcctMgr: "Yes"]
    }


    dimension: TMweb {
      type: string
      sql: CASE WHEN ${delivery_channel_cd} = 'E'  THEN 'Yes'
          else 'No'
          END;;
    }

    # measure: TMweb1 {
    #   label: "TMweb1"
    #   type: count
    #   filters: {
    #     field: TMweb
    #     value: "Yes"
    #   }
    # }

    measure: TMweb1 {
      label: "TMweb1"
      type: count_distinct
      sql: ${total_attendance_ticketId_seqNum}  ;;
      filters: [TMweb: "Yes"]
    }

    dimension: SMS {
      type: string
      sql: CASE WHEN ${delivery_channel_cd} in ('T','U') THEN 'Yes'
          else 'No'
          END;;
    }

    # measure: SMS1 {
    #   label: "SMS1"
    #   type: count
    #   filters: {
    #     field: SMS
    #     value: "Yes"
    #   }
    # }

    measure: SMS1 {
      label: "SMS1"
      type: count_distinct
      sql: ${total_attendance_ticketId_seqNum}  ;;
      filters: [SMS: "Yes"]
    }


    dimension: SH {
      type: string
      sql: CASE WHEN ${delivery_channel_cd} = 'Q' THEN 'Yes'
          else 'No'
          END;;
    }



    # measure: SH1 {
    #   label: "SH1"
    #   type: count
    #   filters: {
    #     field: SH
    #     value: "Yes"
    #   }
    # }

    measure: SH1 {
      label: "SH1"
      type: count_distinct
      sql: ${total_attendance_ticketId_seqNum}  ;;
      filters: [SH: "Yes"]
    }

    dimension: SG {
      type: string
      sql: CASE WHEN ${delivery_channel_cd} in ('S') THEN 'Yes'
          else 'No'
          END;;
    }



    # measure: SG1 {
    #   label: "SG1"
    #   type: count
    #   filters: {
    #     field: SG
    #     value: "Yes"
    #   }
    # }

    measure: SG1 {
      label: "SG1"
      type: count_distinct
      sql: ${total_attendance_ticketId_seqNum}  ;;
      filters: [SG: "Yes"]
    }

    dimension: Gameday {
      type: string
      sql: CASE WHEN ${delivery_channel_cd} = 'Z'  THEN 'Yes'
          else 'No'
          END;;
    }

    # measure: Gameday1 {
    #   label: "Gameday1"
    #   type: count
    #   filters: {
    #     field: Gameday
    #     value: "Yes"
    #   }
    # }

    measure: Gameday1 {
      label: "Gameday1"
      type: count_distinct
      sql: ${total_attendance_ticketId_seqNum}  ;;
      filters: [Gameday: "Yes"]
    }


    dimension: Countios {
      type: string
      sql: CASE WHEN ${fan_device_os_nm} like '%ios' THEN 'Yes'
          else 'No'
          END;;
    }



    # measure: Countios1 {
    #   label: "Countios1"
    #   type: count
    #   filters: {
    #     field: Countios
    #     value: "Yes"
    #   }
    # }

    measure: Countios1 {
      label: "Countios1"
      type: count_distinct
      sql: ${total_attendance_ticketId_seqNum}  ;;
      filters: [Countios: "Yes"]
    }

    dimension: CountAndroid {
      type: string
      sql: CASE WHEN ${fan_device_os_nm} like '%android' THEN 'Yes'
          else 'No'
          END;;
    }



    # measure: CountAndroid1 {
    #   label: "CountAndroid1"
    #   type: count
    #   filters: {
    #     field: CountAndroid
    #     value: "Yes"
    #   }
    # }

    measure: CountAndroid1 {
      label: "CountAndroid1"
      type: count_distinct
      sql: ${total_attendance_ticketId_seqNum}  ;;
      filters: [CountAndroid: "Yes"]
    }


    dimension: CountOther {
      type: string
      sql: CASE WHEN ${fan_device_os_nm} like '%other' THEN 'Yes'
          else 'No'
          END;;
    }



    # measure: CountOther1 {
    #   label: "CountOther1"
    #   type: count
    #   filters: {
    #     field: CountOther
    #     value: "Yes"
    #   }
    # }

    measure: CountOther1 {
      label: "CountOther1"
      type: count_distinct
      sql: ${total_attendance_ticketId_seqNum}  ;;
      filters: [CountOther: "Yes"]
    }

    dimension: RFID {
      type: string
      sql: CASE WHEN ${fan_device_os_nm} = 'smart' THEN 'Yes'
          else 'No'
          END;;
    }



    # measure: RFID1 {
    #   label: "RFID1"
    #   type: count
    #   filters: {
    #     field: RFID
    #     value: "Yes"
    #   }
    # }

    measure: RFID1 {
      label: "RFID1"
      type: count_distinct
      sql: ${total_attendance_ticketId_seqNum}  ;;
      filters: [RFID: "Yes"]
    }

    dimension: CountRejects {
      type: string
      sql: CASE WHEN ${scan_result_cd} like '%REJECT' THEN 'Yes'
          else 'No'
          END;;
    }


    # measure: CountRejects1 {
    #   label: "CountRejects1"
    #   type: count
    #   filters: {
    #     field: CountRejects
    #     value: "Yes"
    #   }
    # }
    measure: CountRejects1 {
      label: "CountRejects1"
      type: count_distinct
      sql: ${total_attendance_ticketId_seqNum}  ;;
      filters: [CountRejects: "Yes"]
    }


    dimension: NFC {
      type: string
      sql: CASE WHEN ${fan_device_os_nm} IN ('IOS','ios', 'android','ANDROID') THEN 'Yes'
          else 'No'
          END;;
    }

    # measure: NFC1 {
    #   label: "NFC1"
    #   type: count
    #   filters: {
    #     field: NFC
    #     value: "Yes"
    #   }
    # }
    measure: NFC1 {
      label: "NFC1"
      type: count_distinct
      sql: ${total_attendance_ticketId_seqNum}  ;;
      filters: [NFC: "Yes"]
    }


    # ----- Sets of fields for drilling ------
    set: detail {
      fields: [
        event_date_time_time,
        event_name,
        event_disp_name,
        major_cat_name,
        minor_cat_name,
        primary_act_name,
        secondary_act_name,
        promoter_name,
        event_type_desc,
        venue_name
      ]
    }
  }
