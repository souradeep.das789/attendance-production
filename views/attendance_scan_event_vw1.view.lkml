view: attendance_scan_event_vw1 {
  sql_table_name: "CLIENT_ANALYTICS"."ATTENDANCE_SCAN_EVENT_VW1"
    ;;

  dimension: access_mgr_sys_id {
    type: string
    sql: ${TABLE}."ACCESS_MGR_SYS_ID" ;;
  }

  dimension: arch_event_code {
    type: string
    sql: ${TABLE}."ARCH_EVENT_CODE" ;;
  }

  dimension: arch_event_id {
    type: string
    sql: ${TABLE}."ARCH_EVENT_ID" ;;
  }

  dimension: archtics_result_cd {
    type: number
    sql: ${TABLE}."ARCHTICS_RESULT_CD" ;;
  }

  dimension: attendance_src_sys_cd {
    type: string
    sql: ${TABLE}."ATTENDANCE_SRC_SYS_CD" ;;
  }

  dimension: barcode {
    type: string
    sql: ${TABLE}."BARCODE" ;;
  }

  dimension: base_tkt_type_id {
    type: string
    sql: ${TABLE}."BASE_TKT_TYPE_ID" ;;
  }

  dimension: bi_dim_event_key {
    type: number
    sql: ${TABLE}."BI_DIM_EVENT_KEY" ;;
  }

  dimension: cancelled_flg {
    type: string
    sql: ${TABLE}."CANCELLED_FLG" ;;
  }

  dimension: client_cd {
    type: string
    sql: ${TABLE}."CLIENT_CD" ;;
  }

  dimension: client_nm {
    type: string
    sql: ${TABLE}."CLIENT_NM" ;;
  }

  dimension: clientbp {
    type: number
    sql: ${TABLE}."CLIENTBP" ;;
  }

  dimension_group: create_dt {
    type: time
    timeframes: [
      raw,
      time,
      date,
      week,
      month,
      quarter,
      year
    ]
    sql: ${TABLE}."CREATE_DT" ;;
  }

  dimension: cur_ind {
    type: string
    sql: ${TABLE}."CUR_IND" ;;
  }

  dimension: cust_id {
    type: string
    sql: ${TABLE}."CUST_ID" ;;
  }

  dimension: delivery_channel_cat {
    type: string
    sql: ${TABLE}."DELIVERY_CHANNEL_CAT" ;;
  }

  dimension: delivery_channel_cd {
    type: string
    sql: ${TABLE}."DELIVERY_CHANNEL_CD" ;;
  }

  dimension: delivery_channel_desc {
    type: string
    sql: ${TABLE}."DELIVERY_CHANNEL_DESC" ;;
  }

  dimension: delivery_channel_nm {
    type: string
    sql: ${TABLE}."DELIVERY_CHANNEL_NM" ;;
  }

  dimension: disc_ven_id {
    type: string
    sql: ${TABLE}."DISC_VEN_ID" ;;
  }

  dimension_group: event_dt {
    type: time
    timeframes: [
      raw,
      time,
      date,
      week,
      month,
      quarter,
      year
    ]
    sql: ${TABLE}."EVENT_DT" ;;
  }

  dimension: event_id_hex {
    type: string
    sql: ${TABLE}."EVENT_ID_HEX" ;;
  }

  dimension: event_id_src_sys_cd {
    type: string
    sql: ${TABLE}."EVENT_ID_SRC_SYS_CD" ;;
  }

  dimension: event_name {
    type: string
    sql: ${TABLE}."EVENT_NAME" ;;
  }

  dimension: event_time {
    type: string
    sql: ${TABLE}."EVENT_TIME" ;;
  }

  dimension: event_type_cd {
    type: string
    sql: ${TABLE}."EVENT_TYPE_CD" ;;
  }

  dimension: event_type_desc {
    type: string
    sql: ${TABLE}."EVENT_TYPE_DESC" ;;
  }

  dimension: exch_active_flg {
    type: string
    sql: ${TABLE}."EXCH_ACTIVE_FLG" ;;
  }

  dimension: fan_device_os_nm {
    type: string
    sql: ${TABLE}."FAN_DEVICE_OS_NM" ;;
  }

  dimension: gate_id {
    type: string
    sql: ${TABLE}."GATE_ID" ;;
  }

  dimension: gate_nm {
    type: string
    sql: ${TABLE}."GATE_NM" ;;
  }

  dimension: ghost_ind {
    type: string
    sql: ${TABLE}."GHOST_IND" ;;
  }

  dimension: host_event_code {
    type: string
    sql: ${TABLE}."HOST_EVENT_CODE" ;;
  }

  dimension: host_event_id {
    type: string
    sql: ${TABLE}."HOST_EVENT_ID" ;;
  }

  dimension_group: insert_ts {
    type: time
    timeframes: [
      raw,
      time,
      date,
      week,
      month,
      quarter,
      year
    ]
    sql: ${TABLE}."INSERT_TS" ;;
  }

  dimension: ism_enabled_flag {
    type: string
    sql: ${TABLE}."ISM_ENABLED_FLAG" ;;
  }

  dimension: major_cat_name {
    type: string
    sql: ${TABLE}."MAJOR_CAT_NAME" ;;
  }

  dimension: minor_cat_name {
    type: string
    sql: ${TABLE}."MINOR_CAT_NAME" ;;
  }

  dimension_group: msg_upload_dttm {
    type: time
    timeframes: [
      raw,
      time,
      date,
      week,
      month,
      quarter,
      year
    ]
    sql: ${TABLE}."MSG_UPLOAD_DTTM" ;;
  }

  dimension: nrep_music_service_flg {
    type: string
    sql: ${TABLE}."NREP_MUSIC_SERVICE_FLG" ;;
  }

  dimension: online_ind {
    type: string
    sql: ${TABLE}."ONLINE_IND" ;;
  }

  dimension_group: onsale_dt {
    type: time
    timeframes: [
      raw,
      time,
      date,
      week,
      month,
      quarter,
      year
    ]
    sql: ${TABLE}."ONSALE_DT" ;;
  }

  dimension: order_system_src_sys_cd {
    type: string
    sql: ${TABLE}."ORDER_SYSTEM_SRC_SYS_CD" ;;
  }

  dimension_group: presale_dt {
    type: time
    timeframes: [
      raw,
      time,
      date,
      week,
      month,
      quarter,
      year
    ]
    sql: ${TABLE}."PRESALE_DT" ;;
  }

  dimension: primary_act_id {
    type: number
    sql: ${TABLE}."PRIMARY_ACT_ID" ;;
  }

  dimension: primary_act_name {
    type: string
    sql: ${TABLE}."PRIMARY_ACT_NAME" ;;
  }

  dimension: primary_ingress_ind {
    type: string
    sql: ${TABLE}."PRIMARY_INGRESS_IND" ;;
  }

  dimension: print_cnt {
    type: number
    sql: ${TABLE}."PRINT_CNT" ;;
  }

  dimension: promoter_name {
    type: string
    sql: ${TABLE}."PROMOTER_NAME" ;;
  }

  dimension: reject_cd {
    type: string
    sql: ${TABLE}."REJECT_CD" ;;
  }

  dimension: reject_reason_nm {
    type: string
    sql: ${TABLE}."REJECT_REASON_NM" ;;
  }

  dimension_group: rotating_entry_token_dttm {
    type: time
    timeframes: [
      raw,
      time,
      date,
      week,
      month,
      quarter,
      year
    ]
    sql: ${TABLE}."ROTATING_ENTRY_TOKEN_DTTM" ;;
  }

  dimension: row_num {
    type: string
    sql: ${TABLE}."ROW_NUM" ;;
  }

  dimension: row_sys_id {
    type: number
    sql: ${TABLE}."ROW_SYS_ID" ;;
  }

  dimension_group: scan_dt_utc {
    type: time
    timeframes: [
      raw,
      date,
      week,
      month,
      quarter,
      year
    ]
    convert_tz: no
    datatype: date
    sql: ${TABLE}."SCAN_DT_UTC" ;;
  }

  dimension_group: scan_dttm_utc {
    type: time
    timeframes: [
      raw,
      time,
      date,
      week,
      month,
      quarter,
      year
    ]
    sql: ${TABLE}."SCAN_DTTM_UTC" ;;
  }

  dimension: scan_entry_cat {
    type: string
    sql: ${TABLE}."SCAN_ENTRY_CAT" ;;
  }

  dimension: scan_event_cd {
    type: string
    sql: ${TABLE}."SCAN_EVENT_CD" ;;
  }

  dimension: scan_event_id {
    type: string
    sql: ${TABLE}."SCAN_EVENT_ID" ;;
  }

  dimension: scan_result_cat {
    type: string
    sql: ${TABLE}."SCAN_RESULT_CAT" ;;
  }

  dimension: scan_result_cd {
    type: string
    sql: ${TABLE}."SCAN_RESULT_CD" ;;
  }

  dimension: scan_result_desc {
    type: string
    sql: ${TABLE}."SCAN_RESULT_DESC" ;;
  }

  dimension: scan_result_nm {
    type: string
    sql: ${TABLE}."SCAN_RESULT_NM" ;;
  }

  dimension: scan_seq_num {
    type: number
    sql: ${TABLE}."SCAN_SEQ_NUM" ;;
  }

  dimension: scan_token_id {
    type: string
    sql: ${TABLE}."SCAN_TOKEN_ID" ;;
  }

  dimension: scanner_device_id {
    type: string
    sql: ${TABLE}."SCANNER_DEVICE_ID" ;;
  }

  dimension: scanner_device_nm {
    type: string
    sql: ${TABLE}."SCANNER_DEVICE_NM" ;;
  }

  dimension: scanner_software_version_num {
    type: string
    sql: ${TABLE}."SCANNER_SOFTWARE_VERSION_NUM" ;;
  }

  dimension: seat_cd {
    type: string
    sql: ${TABLE}."SEAT_CD" ;;
  }

  dimension: seat_num {
    type: string
    sql: ${TABLE}."SEAT_NUM" ;;
  }

  dimension: secondary_act_id {
    type: number
    sql: ${TABLE}."SECONDARY_ACT_ID" ;;
  }

  dimension: secondary_act_name {
    type: string
    sql: ${TABLE}."SECONDARY_ACT_NAME" ;;
  }

  dimension: sect_nm {
    type: string
    sql: ${TABLE}."SECT_NM" ;;
  }

  dimension: settle_cd {
    type: string
    sql: ${TABLE}."SETTLE_CD" ;;
  }

  dimension: ticket_id {
    type: string
    sql: ${TABLE}."TICKET_ID" ;;
  }

  dimension: tkt_status_cd {
    type: string
    sql: ${TABLE}."TKT_STATUS_CD" ;;
  }

  dimension: tkt_status_nm {
    type: string
    sql: ${TABLE}."TKT_STATUS_NM" ;;
  }

  dimension_group: update_ts {
    type: time
    timeframes: [
      raw,
      time,
      date,
      week,
      month,
      quarter,
      year
    ]
    sql: ${TABLE}."UPDATE_TS" ;;
  }

  dimension: ven_id {
    type: string
    sql: ${TABLE}."VEN_ID" ;;
  }

  dimension: ven_id_src_sys_cd {
    type: string
    sql: ${TABLE}."VEN_ID_SRC_SYS_CD" ;;
  }

  dimension: venue_city {
    type: string
    sql: ${TABLE}."VENUE_CITY" ;;
  }

  dimension: venue_name {
    type: string
    sql: ${TABLE}."VENUE_NAME" ;;
  }

  dimension: venue_state {
    type: string
    sql: ${TABLE}."VENUE_STATE" ;;
  }

  dimension: venue_zipcode {
    type: string
    sql: ${TABLE}."VENUE_ZIPCODE" ;;
  }

  dimension: zone_cd {
    type: number
    sql: ${TABLE}."ZONE_CD" ;;
  }

  measure: count {
    type: count
    drill_fields: [detail*]
  }

  # ----- Sets of fields for drilling ------
  set: detail {
    fields: [
      major_cat_name,
      minor_cat_name,
      promoter_name,
      venue_name,
      event_name,
      secondary_act_name,
      primary_act_name
    ]
  }
}
