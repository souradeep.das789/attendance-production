view: attendance_scan_result {
  sql_table_name: "PUBLIC"."ATTENDANCE_SCAN_RESULT"
    ;;

  dimension: archtics_result_cd {
    type: number
    sql: ${TABLE}."ARCHTICS_RESULT_CD" ;;
  }

  dimension: attendance_src_sys_cd {
    type: string
    sql: ${TABLE}."ATTENDANCE_SRC_SYS_CD" ;;
  }

  dimension_group: insert_ts {
    type: time
    timeframes: [
      raw,
      time,
      date,
      week,
      month,
      quarter,
      year
    ]
    sql: ${TABLE}."INSERT_TS" ;;
  }

  dimension: online_ind {
    type: string
    sql: ${TABLE}."ONLINE_IND" ;;
  }

  dimension: scan_entry_cat {
    type: string
    sql: ${TABLE}."SCAN_ENTRY_CAT" ;;
  }

  dimension: scan_result_cat {
    type: string
    sql: ${TABLE}."SCAN_RESULT_CAT" ;;
  }

  dimension: scan_result_cd {
    type: string
    sql: ${TABLE}."SCAN_RESULT_CD" ;;
  }

  dimension: scan_result_desc {
    type: string
    sql: ${TABLE}."SCAN_RESULT_DESC" ;;
  }

  dimension: scan_result_nm {
    type: string
    sql: ${TABLE}."SCAN_RESULT_NM" ;;
  }

  dimension_group: update_ts {
    type: time
    timeframes: [
      raw,
      time,
      date,
      week,
      month,
      quarter,
      year
    ]
    sql: ${TABLE}."UPDATE_TS" ;;
  }

  measure: count {
    type: count
    drill_fields: []
  }
}
