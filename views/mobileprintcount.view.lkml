view: mobileprintcount {
  derived_table: {
    sql: SELECT
          scan."DELIVERY_CHANNEL_CAT"  AS "attendance_scan_event_vw.delivery_channel_cat",
          scan.HOST_EVENT_ID as "host_event_id",
          COUNT(scan.ticket_id)
          from
          BI.PUBLIC.ATTENDANCE_SCAN_EVENT_VW scan
              INNER JOIN
          (SELECT
              se.ticket_id,
           scan_event_id,
          MAX(se.SCAN_DTTM_UTC) AS lastValidScanDate
          FROM
              BI.PUBLIC.ATTENDANCE_SCAN_EVENT_VW se
          WHERE
              {% condition event_name %} se.event_name  {% endcondition %}
    AND se.SCAN_ENTRY_CAT IN ('ENTRY' , 'REENTRY', 'EXIT', 'INTERNAL', 'INTERNAL_ENTRY', 'INTERNAL_ENTRY_EXIT')
    and {% condition event_date %} se.event_dt  {% endcondition %}
    AND se.ATTENDANCE_SRC_SYS_CD = 'PRESENCE'

    and {% condition venuename %} se.Venue_Name  {% endcondition %}


    and {% condition eventtypedesc %} se.Event_Type_Desc  {% endcondition %}


    and {% condition majorcatname %} se.Major_Cat_Name  {% endcondition %}

    and {% condition minorcatname %} se.Minor_Cat_Name  {% endcondition %}
    and (((case when (se."FAN_DEVICE_OS_NM")  LIKE '%rotating%'  then 'Yes'
    else
    case when (se."FAN_DEVICE_OS_NM") IN ('android' , 'ANDROID', 'IOS', 'ios') then 'Yes'
    else 'No'
    end
    end) = {% parameter safetix %} ))
          AND se.ATTENDANCE_SRC_SYS_CD = 'PRESENCE'
          GROUP BY 1,2
          ) AS lastScan
          ON scan.scan_event_id = lastScan.scan_event_id
              AND scan.ticket_id = lastScan.ticket_id
              AND scan.SCAN_DTTM_UTC = lastScan.lastValidScanDate
      GROUP BY 1,2
       ;;
  }
  filter: event_name {
    type: string
  }

  filter: event_date {
    type: date

  }
  filter: venuename {
    type: string
  }

  filter: safetix {
    type: string
    default_value: "No"
  }

  filter: eventtypedesc {
    type: string

  }
  filter: majorcatname {
    type: string

  }
  filter: minorcatname {
    type: string

  }
  measure: count {
    type: count
    drill_fields: [detail*]
  }

  dimension: attendance_scan_event_vw_delivery_channel_cat {
    type: string
    sql: ${TABLE}."attendance_scan_event_vw.delivery_channel_cat" ;;
  }
  dimension: host_event_id {
    type: string
    sql: ${TABLE}."host_event_id" ;;
  }
  dimension: countscan_ticket_id {
    type: number
    sql: ${TABLE}."COUNT(SCAN.TICKET_ID)" ;;
  }

  set: detail {
    fields: [attendance_scan_event_vw_delivery_channel_cat, countscan_ticket_id]
  }
}
