view: bi_dim_event {
  sql_table_name: "CLIENT_ANALYTICS"."BI_DIM_EVENT"
    ;;

  dimension: access_priority_id {
    type: number
    sql: ${TABLE}."ACCESS_PRIORITY_ID" ;;
  }

  dimension: arch_event_code {
    type: string
    sql: ${TABLE}."ARCH_EVENT_CODE" ;;
  }

  dimension: arch_event_id {
    type: string
    sql: ${TABLE}."ARCH_EVENT_ID" ;;
  }

  dimension: bi_dim_event_key {
    type: number
    sql: ${TABLE}."BI_DIM_EVENT_KEY" ;;
  }

  dimension: bi_dim_venue_fk {
    type: number
    sql: ${TABLE}."BI_DIM_VENUE_FK" ;;
  }

  dimension: box_office_sales_flg {
    type: string
    sql: ${TABLE}."BOX_OFFICE_SALES_FLG" ;;
  }

  dimension: cancelled_flg {
    type: string
    sql: ${TABLE}."CANCELLED_FLG" ;;
  }

  dimension: client_cd {
    type: string
    sql: ${TABLE}."CLIENT_CD" ;;
  }

  dimension: clientbp {
    type: number
    sql: ${TABLE}."CLIENTBP" ;;
  }

  dimension: crcy_cd {
    type: string
    sql: ${TABLE}."CRCY_CD" ;;
  }

  dimension_group: create_dt {
    type: time
    timeframes: [
      raw,
      time,
      date,
      week,
      month,
      quarter,
      year
    ]
    sql: ${TABLE}."CREATE_DT" ;;
  }

  dimension: credit_amt {
    type: number
    sql: ${TABLE}."CREDIT_AMT" ;;
  }

  dimension: credit_enabled_flg {
    type: string
    sql: ${TABLE}."CREDIT_ENABLED_FLG" ;;
  }

  dimension: cur_ind {
    type: string
    sql: ${TABLE}."CUR_IND" ;;
  }

  dimension: days_onsale {
    type: number
    sql: ${TABLE}."DAYS_ONSALE" ;;
  }

  dimension: days_presale {
    type: number
    sql: ${TABLE}."DAYS_PRESALE" ;;
  }

  dimension: disc_event_id {
    type: string
    sql: ${TABLE}."DISC_EVENT_ID" ;;
  }

  dimension: event_code {
    type: string
    sql: ${TABLE}."EVENT_CODE" ;;
  }

  dimension: event_disp_nm {
    type: string
    sql: ${TABLE}."EVENT_DISP_NM" ;;
  }

  dimension_group: event_dt {
    type: time
    timeframes: [
      raw,
      time,
      date,
      week,
      month,
      quarter,
      year
    ]
    sql: ${TABLE}."EVENT_DT" ;;
  }

  dimension: event_id {
    type: string
    sql: ${TABLE}."EVENT_ID" ;;
  }

  dimension: event_id_hex {
    type: string
    sql: ${TABLE}."EVENT_ID_HEX" ;;
  }

  dimension: event_id_src_sys_cd {
    type: string
    sql: ${TABLE}."EVENT_ID_SRC_SYS_CD" ;;
  }

  dimension: event_name {
    type: string
    sql: ${TABLE}."EVENT_NAME" ;;
  }

  dimension: event_nm_typ {
    type: string
    sql: ${TABLE}."EVENT_NM_TYP" ;;
  }

  dimension: event_time {
    type: string
    sql: ${TABLE}."EVENT_TIME" ;;
  }

  dimension: event_time_civ {
    type: string
    sql: ${TABLE}."EVENT_TIME_CIV" ;;
  }

  dimension: event_type_cd {
    type: string
    sql: ${TABLE}."EVENT_TYPE_CD" ;;
  }

  dimension: event_type_desc {
    type: string
    sql: ${TABLE}."EVENT_TYPE_DESC" ;;
  }

  dimension: exch_active_flg {
    type: string
    sql: ${TABLE}."EXCH_ACTIVE_FLG" ;;
  }

  dimension: exch_enabled {
    type: string
    sql: ${TABLE}."EXCH_ENABLED" ;;
  }

  dimension_group: host_delete_dt {
    type: time
    timeframes: [
      raw,
      time,
      date,
      week,
      month,
      quarter,
      year
    ]
    sql: ${TABLE}."HOST_DELETE_DT" ;;
  }

  dimension: host_sys_cd {
    type: string
    sql: ${TABLE}."HOST_SYS_CD" ;;
  }

  dimension_group: insert_ts {
    type: time
    timeframes: [
      raw,
      time,
      date,
      week,
      month,
      quarter,
      year
    ]
    sql: ${TABLE}."INSERT_TS" ;;
  }

  dimension: is_arch_deployed {
    type: number
    sql: ${TABLE}."IS_ARCH_DEPLOYED" ;;
  }

  dimension: is_archtics_event {
    type: string
    sql: ${TABLE}."IS_ARCHTICS_EVENT" ;;
  }

  dimension: is_resale_enabled {
    type: number
    sql: ${TABLE}."IS_RESALE_ENABLED" ;;
  }

  dimension: ism_enabled_flag {
    type: string
    sql: ${TABLE}."ISM_ENABLED_FLAG" ;;
  }

  dimension: kafka_cluster_nm {
    type: string
    sql: ${TABLE}."KAFKA_CLUSTER_NM" ;;
  }

  dimension_group: kafka_create_ts {
    type: time
    timeframes: [
      raw,
      time,
      date,
      week,
      month,
      quarter,
      year
    ]
    sql: ${TABLE}."KAFKA_CREATE_TS" ;;
  }

  dimension: kafka_offset_num {
    type: number
    sql: ${TABLE}."KAFKA_OFFSET_NUM" ;;
  }

  dimension: kafka_partition_num {
    type: number
    sql: ${TABLE}."KAFKA_PARTITION_NUM" ;;
  }

  dimension: kafka_topic_nm {
    type: string
    sql: ${TABLE}."KAFKA_TOPIC_NM" ;;
  }

  dimension: major_cat_name {
    type: string
    sql: ${TABLE}."MAJOR_CAT_NAME" ;;
  }

  dimension: minor_cat_name {
    type: string
    sql: ${TABLE}."MINOR_CAT_NAME" ;;
  }

  dimension: nrep_music_service_flg {
    type: string
    sql: ${TABLE}."NREP_MUSIC_SERVICE_FLG" ;;
  }

  dimension: olson_tz_cd {
    type: string
    sql: ${TABLE}."OLSON_TZ_CD" ;;
  }

  dimension_group: onsale_dt {
    type: time
    timeframes: [
      raw,
      time,
      date,
      week,
      month,
      quarter,
      year
    ]
    sql: ${TABLE}."ONSALE_DT" ;;
  }

  dimension_group: presale_dt {
    type: time
    timeframes: [
      raw,
      time,
      date,
      week,
      month,
      quarter,
      year
    ]
    sql: ${TABLE}."PRESALE_DT" ;;
  }

  dimension_group: presale_off_dt {
    type: time
    timeframes: [
      raw,
      time,
      date,
      week,
      month,
      quarter,
      year
    ]
    sql: ${TABLE}."PRESALE_OFF_DT" ;;
  }

  dimension: primary_act_id {
    type: number
    sql: ${TABLE}."PRIMARY_ACT_ID" ;;
  }

  dimension: primary_act_name {
    type: string
    sql: ${TABLE}."PRIMARY_ACT_NAME" ;;
  }

  dimension: promoter_name {
    type: string
    sql: ${TABLE}."PROMOTER_NAME" ;;
  }

  dimension: refund_enabled_flg {
    type: string
    sql: ${TABLE}."REFUND_ENABLED_FLG" ;;
  }

  dimension: row_sys_id {
    type: number
    sql: ${TABLE}."ROW_SYS_ID" ;;
  }

  dimension: search_string {
    type: string
    sql: ${TABLE}."SEARCH_STRING" ;;
  }

  dimension: secondary_act_id {
    type: number
    sql: ${TABLE}."SECONDARY_ACT_ID" ;;
  }

  dimension: secondary_act_name {
    type: string
    sql: ${TABLE}."SECONDARY_ACT_NAME" ;;
  }

  dimension: settle_cd {
    type: string
    sql: ${TABLE}."SETTLE_CD" ;;
  }

  dimension: src_event_id {
    type: string
    sql: ${TABLE}."SRC_EVENT_ID" ;;
  }

  dimension_group: td_insert_ts {
    type: time
    timeframes: [
      raw,
      time,
      date,
      week,
      month,
      quarter,
      year
    ]
    sql: ${TABLE}."TD_INSERT_TS" ;;
  }

  dimension_group: td_update_ts {
    type: time
    timeframes: [
      raw,
      time,
      date,
      week,
      month,
      quarter,
      year
    ]
    sql: ${TABLE}."TD_UPDATE_TS" ;;
  }

  dimension: tmone_visible_cd {
    type: number
    sql: ${TABLE}."TMONE_VISIBLE_CD" ;;
  }

  dimension_group: update_ts {
    type: time
    timeframes: [
      raw,
      time,
      date,
      week,
      month,
      quarter,
      year
    ]
    sql: ${TABLE}."UPDATE_TS" ;;
  }

  dimension: valid_event_flag {
    type: string
    sql: ${TABLE}."VALID_EVENT_FLAG" ;;
  }

  dimension: ven_id {
    type: string
    sql: ${TABLE}."VEN_ID" ;;
  }

  dimension: ven_id_src_sys_cd {
    type: string
    sql: ${TABLE}."VEN_ID_SRC_SYS_CD" ;;
  }

  dimension: venue_city {
    type: string
    sql: ${TABLE}."VENUE_CITY" ;;
  }

  dimension: venue_name {
    type: string
    sql: ${TABLE}."VENUE_NAME" ;;
  }

  dimension: venue_st_cd {
    type: string
    sql: ${TABLE}."VENUE_ST_CD" ;;
  }

  dimension: venue_state {
    type: string
    sql: ${TABLE}."VENUE_STATE" ;;
  }

  dimension: venue_zipcode {
    type: string
    sql: ${TABLE}."VENUE_ZIPCODE" ;;
  }

  dimension: zone_cd {
    type: number
    sql: ${TABLE}."ZONE_CD" ;;
  }

  measure: count {
    type: count
    drill_fields: [detail*]
  }

  # ----- Sets of fields for drilling ------
  set: detail {
    fields: [
      major_cat_name,
      venue_name,
      secondary_act_name,
      primary_act_name,
      promoter_name,
      minor_cat_name,
      event_name
    ]
  }
}
