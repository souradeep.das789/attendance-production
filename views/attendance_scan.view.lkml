view: attendance_scan {
  sql_table_name: "PUBLIC"."ATTENDANCE_SCAN"
    ;;
  drill_fields: [attendance_scan_id]

  dimension: attendance_scan_id {
    primary_key: yes
    type: number
    sql: ${TABLE}."ATTENDANCE_SCAN_ID" ;;
  }

  dimension: access_mgr_sys_id {
    type: string
    sql: ${TABLE}."ACCESS_MGR_SYS_ID" ;;
  }

  dimension: arch_event_id {
    type: string
    sql: ${TABLE}."ARCH_EVENT_ID" ;;
  }

  dimension: attendance_src_sys_cd {
    type: string
    sql: ${TABLE}."ATTENDANCE_SRC_SYS_CD" ;;
  }

  dimension: barcode {
    type: string
    sql: ${TABLE}."BARCODE" ;;
  }

  dimension: base_tkt_type_id {
    type: string
    sql: ${TABLE}."BASE_TKT_TYPE_ID" ;;
  }

  dimension: client_cd {
    type: string
    sql: ${TABLE}."CLIENT_CD" ;;
  }

  dimension: client_nm {
    type: string
    sql: ${TABLE}."CLIENT_NM" ;;
  }

  dimension: cust_id {
    type: string
    sql: ${TABLE}."CUST_ID" ;;
  }

  dimension: delivery_channel_cd {
    type: string
    sql: ${TABLE}."DELIVERY_CHANNEL_CD" ;;
  }

  dimension: disc_ven_id {
    type: string
    sql: ${TABLE}."DISC_VEN_ID" ;;
  }

  dimension: event_cd {
    type: string
    sql: ${TABLE}."EVENT_CD" ;;
  }

  dimension_group: event_dt {
    type: time
    timeframes: [
      raw,
      date,
      week,
      month,
      quarter,
      year
    ]
    convert_tz: no
    datatype: date
    sql: ${TABLE}."EVENT_DT" ;;
  }

  dimension: event_id_hex {
    type: string
    sql: ${TABLE}."EVENT_ID_HEX" ;;
  }

  dimension: fan_device_os_nm {
    type: string
    sql: ${TABLE}."FAN_DEVICE_OS_NM" ;;
  }

  dimension: gate_id {
    type: string
    sql: ${TABLE}."GATE_ID" ;;
  }

  dimension: gate_nm {
    type: string
    sql: ${TABLE}."GATE_NM" ;;
  }

  dimension_group: insert_ts {
    type: time
    timeframes: [
      raw,
      time,
      date,
      week,
      month,
      quarter,
      year
    ]
    sql: ${TABLE}."INSERT_TS" ;;
  }

  dimension_group: msg_upload_dttm {
    type: time
    timeframes: [
      raw,
      time,
      date,
      week,
      month,
      quarter,
      year
    ]
    sql: ${TABLE}."MSG_UPLOAD_DTTM" ;;
  }

  dimension_group: ods_ts {
    type: time
    timeframes: [
      raw,
      time,
      date,
      week,
      month,
      quarter,
      year
    ]
    sql: ${TABLE}."ODS_TS" ;;
  }

  dimension: order_system_src_sys_cd {
    type: string
    sql: ${TABLE}."ORDER_SYSTEM_SRC_SYS_CD" ;;
  }

  dimension: print_cnt {
    type: number
    sql: ${TABLE}."PRINT_CNT" ;;
  }

  dimension: reject_cd {
    type: string
    sql: ${TABLE}."REJECT_CD" ;;
  }

  dimension: reject_reason_nm {
    type: string
    sql: ${TABLE}."REJECT_REASON_NM" ;;
  }

  dimension_group: rotating_entry_token_dttm {
    type: time
    timeframes: [
      raw,
      time,
      date,
      week,
      month,
      quarter,
      year
    ]
    sql: ${TABLE}."ROTATING_ENTRY_TOKEN_DTTM" ;;
  }

  dimension: row_num {
    type: string
    sql: ${TABLE}."ROW_NUM" ;;
  }

  dimension_group: scan_dt_utc {
    type: time
    timeframes: [
      raw,
      date,
      week,
      month,
      quarter,
      year
    ]
    convert_tz: no
    datatype: date
    sql: ${TABLE}."SCAN_DT_UTC" ;;
  }

  dimension_group: scan_dttm_utc {
    type: time
    timeframes: [
      raw,
      time,
      date,
      week,
      month,
      quarter,
      year
    ]
    sql: ${TABLE}."SCAN_DTTM_UTC" ;;
  }

  dimension: scan_event_id {
    type: string
    sql: ${TABLE}."SCAN_EVENT_ID" ;;
  }

  dimension: scan_event_src_sys_cd {
    type: string
    sql: ${TABLE}."SCAN_EVENT_SRC_SYS_CD" ;;
  }

  dimension: scan_result_cd {
    type: string
    sql: ${TABLE}."SCAN_RESULT_CD" ;;
  }

  dimension: scan_seq_num {
    type: number
    sql: ${TABLE}."SCAN_SEQ_NUM" ;;
  }

  dimension: scan_token_id {
    type: string
    sql: ${TABLE}."SCAN_TOKEN_ID" ;;
  }

  dimension: scanner_device_id {
    type: string
    sql: ${TABLE}."SCANNER_DEVICE_ID" ;;
  }

  dimension: scanner_device_nm {
    type: string
    sql: ${TABLE}."SCANNER_DEVICE_NM" ;;
  }

  dimension: scanner_software_version_num {
    type: string
    sql: ${TABLE}."SCANNER_SOFTWARE_VERSION_NUM" ;;
  }

  dimension: seat_cd {
    type: string
    sql: ${TABLE}."SEAT_CD" ;;
  }

  dimension: seat_num {
    type: string
    sql: ${TABLE}."SEAT_NUM" ;;
  }

  dimension: sect_nm {
    type: string
    sql: ${TABLE}."SECT_NM" ;;
  }

  dimension: ticket_id {
    type: string
    sql: ${TABLE}."TICKET_ID" ;;
  }

  dimension: tkt_status_cd {
    type: string
    sql: ${TABLE}."TKT_STATUS_CD" ;;
  }

  dimension_group: update_ts {
    type: time
    timeframes: [
      raw,
      time,
      date,
      week,
      month,
      quarter,
      year
    ]
    sql: ${TABLE}."UPDATE_TS" ;;
  }

  measure: count {
    type: count
    drill_fields: [attendance_scan_id]
  }
}
