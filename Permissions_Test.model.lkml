connection: "snowflake"

include: "/views/*.view.lkml"                # include all views in the views/ folder in this project
# include: "/**/view.lkml"                   # include all views in this project
# include: "my_dashboard.dashboard.lookml"   # include a LookML dashboard called my_dashboard

# # Select the views that should be a part of this model,
# # and define the joins that connect them together.
#
# explore: order_items {
#   join: orders {
#     relationship: many_to_one
#     sql_on: ${orders.id} = ${order_items.order_id} ;;
#   }
#
#   join: users {
#     relationship: many_to_one
#     sql_on: ${users.id} = ${orders.user_id} ;;
#   }
# }
explore: attendance_scan {

  join: attendance_event_xref {
    type: inner
    sql_on: ${attendance_scan.scan_event_id} = ${attendance_event_xref.scan_event_id} ;;
    relationship: one_to_many
  }

  join: bi_dim_event {
    type: inner
    sql_on: ${bi_dim_event.event_id_hex} = ${attendance_event_xref.event_id_hex}
          and ${bi_dim_event.arch_event_id} = ${attendance_event_xref.arch_event_id}
          and ${bi_dim_event.client_cd} = ${attendance_event_xref.client_cd};;
    relationship: many_to_one
  }
  join: attendance_scan_result {
    type: inner {
      sql_on: ${attendance_scan.scan_result_cd} = ${attendance_scan_result.scan_result_cd}
        AND ${attendance_scan.attendance_src_sys_cd} = ${attendance_scan_result.attendance_src_sys_cd};;
      relationship: one_to_many
    }
  }
  join: attendance_delivery_channel {
    type: inner
    sql_on: ${attendance_scan.delivery_channel_cd} = ${attendance_delivery_channel.delivery_channel_cd} ;;
    relationship: many_to_one

  }
  join: attendance_tkt_status {
    type: left_outer
    sql_on: ${attendance_scan.tkt_status_cd} = ${attendance_tkt_status.tkt_status_cd} ;;
    relationship: many_to_one
  }

}
